/*
  File: Texture.h

  Description: It contains the textured image that needs to be mapped onto an image.
  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013
*/

#ifndef TEXTURE_H
#define TEXTURE_H

#include<iostream>
#include "png++/png.hpp"

using namespace std;

namespace sivelab {
  
  class Texture {
  public:
    

    string sourceFile,name,type;
    png::image< png::rgb_pixel > image;
    
    
    Texture();
  };
}

#endif