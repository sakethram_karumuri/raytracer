/*
  File: UVWbasis.cpp

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013

Description: This class is reponsible for generating a UVW basis. It is generated using a t(0,1,0) vector.
*/


#include "UVWbasis.h"

using namespace sivelab;

UVWbasis::UVWbasis() {
  // cout<<"Camera's default position:"<<camPosition<<endl;
  u.set(1,0,0);
  v.set(0,1,0);
  w.set(0,0,-1);

  // cout<<"u:"<<u<<endl<<"v:"<<v<<endl<<"w:"<<w<<endl;
}

UVWbasis::UVWbasis(Vector3D camPos, Vector3D lookatPt, Vector3D viewDirec) {

  // cout<<"Camera's UVWbasis origin:"<<camPos<<endl;
  // cout<<"Camera's viewDir:"<<viewDirec<<endl;

  if(viewDirec[0]==0&&viewDirec[1]==0&&viewDirec[2]==0){
    //Calculating a vector
    // cout<<"Camera's looking at:"<<lookatPt<<endl;
    lookatRay = lookatPt-camPos;
  }
  else
    lookatRay = viewDirec;


  //cout<<"Camera's look at ray:"<<lookatRay<<endl;

  //cout<<"Camera's normalized look at ray:"<<lookatRay<<endl;
  //cout<<"It's length is:"<<lookatRay.length()<<endl;

  //Constructing w vector from lookatRay
  w=(-1.0)*lookatRay;
  lenw = w.normalize();

  //Constructing t similar to w but not collinear

  //temp=w;
  temp.set(0,1,0);
  //Modifying temp vector

  //temp[2]+=1;

  //Creating u
  u= temp.cross(w);
  lenu = u.normalize();

  //Creating v
  v = w.cross(u);
  lenv = v.normalize();

  // cout<<"u:"<<u<<endl<<"v:"<<v<<endl<<"w:"<<w<<endl;

}


UVWbasis::UVWbasis(Vector3D camPos, Vector3D viewDirec) {
  
    lookatRay = viewDirec;
//Constructing w vector from lookatRay
  w=(-1.0)*lookatRay;
  lenw = w.normalize();

  //Constructing t similar to w but not collinear

  
  if(viewDirec[0]==0 && (viewDirec[1]==-1||viewDirec[1]==1) && viewDirec[2]==0) {
   //cout<<"Hello"<<endl;
    temp.set(1,0,0);
  }
  else
    temp.set(0,1,0);
  //Modifying temp vector

  //temp[2]+=1;

  //Creating u
  u= temp.cross(w);
  lenu = u.normalize();

  //Creating v
  v = w.cross(u);
  lenv = v.normalize();

  // cout<<"u:"<<u<<endl<<"v:"<<v<<endl<<"w:"<<w<<endl;

}

