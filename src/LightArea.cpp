#include "LightArea.h"

using namespace sivelab;

LightArea::LightArea() {
}

LightArea::LightArea(Vector3D pos, Vector3D inten, string ty,Vector3D norm) {

position=pos;
intensity=inten;
type=ty;
normal=norm;

basis = new UVWbasis(pos,normal);

}