/*
  File: Lambertian.h

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013
*/


#ifndef LAMBERTIAN_H
#define LAMBERTIAN_H

#include<iostream>
#include "HitStructure.h"
#include "Ray.h"
#include "Vector3D.h"
#include "Scene.h"
#include "Shader.h"
#include "RenderObject.h"


using namespace std;

namespace sivelab {
  
  class Lambertian: public Shader {
  public:
    Scene *sc;
    
    Lambertian();
    Lambertian(string,string,Vector3D,string);
    Vector3D applyShading(Ray *,double,double,HitStructure *,Vector3D);
    
    
  };
   
  
}

#endif
