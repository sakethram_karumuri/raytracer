/*
  File: Shader.cpp

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013

Description: This class has various constructors that are used for creating different shaders.
*/


#include<iostream>
#include "Shader.h"

using namespace sivelab;

Shader::Shader() {
  //cout<<"Shader object created"<<endl;

}

Shader::Shader(string nam,string ty, Vector3D diff) {
  name=nam;
  type=ty;
  diffuse=diff;
  //cout<<"Shader object created"<<endl;
}

Shader::Shader(string nam, string ty, Vector3D diff, Vector3D spec, float phongExp) {

  name=nam;
  type=ty;
  diffuse=diff;
  specular=spec;
  phExp=phongExp;

  //cout<<"Shader object created"<<endl;
  //cout<<"Shader details:\n"<<"Name:"<<name<<"\nType:"<<type<<"\nDiffuse:"<<diffuse;
  //cout<<"\nSpecular:"<<specular<<endl;
}

Shader::Shader(string nam,string ty) {
  name=nam;
  type=ty;
  
}

Shader::Shader(string nam,string ty, Vector3D diff, double mirr) {
  name = nam;
  type= ty;
  diffuse = diff;
  mirrorCoef = mirr;
}

Shader::Shader(string nam,string ty, Vector3D diff, Vector3D spec, float phEx, double mirr) {
  name = nam;
  type= ty;
  diffuse = diff;
  specular = spec;
  phExp = phEx;
  mirrorCoef = mirr;
}
