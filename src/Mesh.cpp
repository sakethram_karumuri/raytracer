/*
  File: Mesh.cpp

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013

Description: This class has intersection function for a sphere. Its checks the roots with tmax values and assigns tmax accordingly.
*/


#include "Mesh.h"
#include "model_obj.h"
#include "Vector3D.h"

using namespace sivelab;

void Mesh::createBBox() {

  //cout<<"Inside mesh bbox"<<endl;
  Vector3D minPoint,maxPoint;
  //minPoint.set(9999,9999,9999);
  //maxPoint.set(-9999,-9999,-9999);
  //cout<<"Min point: "<<minPoint<<" Max point: "<<maxPoint<<endl;
  for(int i=0;i<meshTriangles.size();i++) {
  
    //cout<<"triangles vertices:"<<meshTriangles[i]->v0<<" "<<meshTriangles[i]->v1<<endl;
    if(meshTriangles[i]->bb->minPt[0] < minPoint[0])
      minPoint[0]=meshTriangles[i]->bb->minPt[0];
    if(meshTriangles[i]->bb->minPt[1] < minPoint[1])
      minPoint[1]=meshTriangles[i]->bb->minPt[1];
    if(meshTriangles[i]->bb->minPt[2] < minPoint[2])
      minPoint[2]=meshTriangles[i]->bb->minPt[2];
    
    if(meshTriangles[i]->bb->maxPt[0] > maxPoint[0])
      maxPoint[0]=meshTriangles[i]->bb->maxPt[0];
    if(meshTriangles[i]->bb->maxPt[1] > maxPoint[1])
      maxPoint[1]=meshTriangles[i]->bb->maxPt[1];
    if(meshTriangles[i]->bb->maxPt[2] > maxPoint[2])
      maxPoint[2]=meshTriangles[i]->bb->maxPt[2];
        
  }
  //cout<<"Min point: "<<minPoint<<" Max point: "<<maxPoint<<endl;
  
  bb = new BoundingBox(minPoint,maxPoint);
}

bool Mesh::intersect(Ray *r, double& tmin, double& tmax, HitStructure *hs) {

   Scene *sc=Scene::getInstance();
  bool hit;
if(sc->bvh=="no") {
    for(int i=0;i<meshTriangles.size();i++) {
       hit = meshTriangles[i]->intersect(r,tmin,tmax,hs);
   }
}

else {
  hit = meshRoot->intersect(r,tmin,tmax,hs);
}
  return hit;
}

void Mesh::load(std::string &filename)
{ 
  std::vector<int> indexList;
  std::vector<Vector3D> vertexList;
  
  //std::cerr << "Parsing render file: " << filename << "..." << std::endl;
  
  ModelOBJ mOBJ;
  if (mOBJ.import( filename.c_str() )){
    //std::cout << "...loading successful." << std::endl;
  }
  else 
    {
      //std::cout << "...unsuccessful! Exiting!" << std::endl;
      exit(EXIT_FAILURE);
    }

  //std::cout << "Number of meshes contained within OBJ: " << mOBJ.getNumberOfMeshes() << std::endl;
  //std::cout << "Number of triangles contained within OBJ: " << mOBJ.getNumberOfTriangles() << std::endl;

  const ModelOBJ::Mesh *pMesh = 0;
  const ModelOBJ::Material *pMaterial = 0;
  const ModelOBJ::Vertex *pVertices = 0;

  const int *idxBuffer = mOBJ.getIndexBuffer();
  for (int mIdx=0; mIdx<mOBJ.getNumberOfMeshes(); mIdx++)
    {
      pMesh = &mOBJ.getMesh(mIdx);
      pMaterial = pMesh->pMaterial;
      pVertices = mOBJ.getVertexBuffer();
    
      // Would be good to look over the material structure so that you
      // can create the correct shader here for the mesh
      // 
      // pMaterial->diffuse[0], pMaterial->diffuse[1], pMaterial->diffuse[2], etc...

      for (int i=pMesh->startIndex; i<(pMesh->startIndex + pMesh->triangleCount*3); i+=3)
	{
	  ModelOBJ::Vertex v0, v1, v2;
	  v0 = pVertices[ idxBuffer[i] ];
	  v1 = pVertices[ idxBuffer[i+1] ];
	  v2 = pVertices[ idxBuffer[i+2] ];
	  
	  Shape *tr = new Triangle();
	  tr->name=name;
	  tr->type="triangle";
	  tr->ref=ref;
	  tr->shaderRef=shaderRef;
	  //Vector3D v1,v1,v2;
	  
	  tr->v0.set(v0.position[0],v0.position[1],v0.position[2]);
	  tr->v1.set(v1.position[0],v1.position[1],v1.position[2]);
	  tr->v2.set(v2.position[0],v2.position[1],v2.position[2]);
	  
	  tr->createBBox();
	  meshTriangles.push_back(tr);
	  
	  //cout<<meshTriangles.size()<<endl;
	//cout<<"Hello"<<endl;
	  // ModelOBJ Vertex follows this format: v0.position[0],
	  // v0.position[1], v0.position[2]

	  // You can also access the normals too v0.normal[0],
	  // v0.normal[1], v0.normal[2]. Notice that these are part of
	  // the ModelOBJ::Vertex structure.
	}      
    }
    meshRoot = new BVHNode(meshTriangles,0);
    createBBox();
    //cout<<"hello"<<endl;
}