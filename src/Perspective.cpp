/*
  File: Perspective.cpp

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 3
  Date: 11/19/2013

Description: This class has various constructors that are used for creating different shaders.
*/


#include<iostream>
#include "Perspective.h"
#include "Scene.h"
#include "RenderObject.h"
#include "HitStructure.h"
#include "math.h"

using namespace sivelab;

Perspective::Perspective() {
  //cout<<"Shader object created"<<endl;

}

Perspective::Perspective(string ty,Vector3D pos, Vector3D viewdr, Vector3D lookatpt, float focalLn, float imgPW, double wid, double hei) {

  //Declaration of variables.
  type=ty;
  position=pos;
  viewDir=viewdr;
  lookatPoint=lookatpt;
  focalLength=focalLn;
  imagePlaneWidth=imgPW;
  pixwidth=wid;
  pixheight=hei;

  //Calculation of aspect ratio
  aspratio = (float)pixwidth/(float)pixheight;
  imagePlaneHeight=imagePlaneWidth/aspratio;

  basis = new UVWbasis(position,lookatpt,viewDir);
}

void Perspective::computeRay(int nx,int ny,float wid,float hei)
{
  double l=(-wid)/2.0;
  double r=wid/2.0;
  double t=hei/2.0;
  double b=(-hei)/2.0;
  double tu,tv;
  double red,green,blue;
  bool hit=false;
  int count=0; //Tells how many pixels are hit in the final image.
  int firstObj; //Holds the reference of the object that was first hit.
  Vector3D bgColor;
  
  png::image< png::rgb_pixel > imData( nx, ny );
  RenderObject *ro = new RenderObject();
  Scene *sc = Scene::getInstance();
  //For each pixel
  int rpp = sc->rpp;
  int n = sqrt(rpp);
  for(size_t j=0;j<ny;j++)
    {
      for(size_t i=0;i<nx;i++)
	{
	  Vector3D pixelColor;
	  for(int p=0; p<=n-1; p++) {
	    for(int q=0; q<=n-1; q++) {
	      Vec2D tempvec,test;
	      tempvec.x = ((p+drand48())/n);
	      tempvec.y = ((q+drand48())/n);
	      sc->glossVector.push_back(tempvec);
	    }
	  }
	  
	  for(int p=0; p<=n-1; p++) {
	    for(int q=0; q<=n-1; q++) {
	      Vec2D tempvec2 = sc->glossVector.back();
	      //cout<<sc->glossVector.back().x<<endl;
	  tu=l+(r-l)*(i+tempvec2.x)/(float)nx;
	  tv=b+(t-b)*(j+tempvec2.y)/(float)ny;
	  
	 //tu=l+(r-l)*(i+0.5)/(float)nx;
	 //tv=b+(t-b)*(j+0.5)/(float)ny;

	  HitStructure *hs= new HitStructure();
	  Ray *r= new Ray();
	  r->origin = position;
	  r->direction = (-1)*focalLength*basis->w + tu*basis->u + tv*basis->v;
	  sc->depth = 8;
	  double tmin = 1.0;
	  double tmax = numeric_limits<double>::max();
	  
	  firstObj = ro->intersect(r,tmin,tmax,hs);
	 
	  if(tmax < numeric_limits<double>::max()){
	  //cout<<"Point of contact is: "<<hs->ptContact<<endl;
	    hs->firstObjref=firstObj;
	    ro->applyShader(r,hs,position);
	    count++;
	    pixelColor += hs->capL;
	    //imData[ny-j-1][i] = png::rgb_pixel(hs->capL[0]*255,hs->capL[1]*255,hs->capL[2]*255);
	  }
	  else {
	    
	    bgColor = sc->enviMapping(r);
	    pixelColor += sc->enviMapping(r);
	    //imData[ny-j-1][i] = png::rgb_pixel(bgColor[0]*255,bgColor[1]*255,bgColor[2]*255);
	    sc->glossVector.pop_back();
	  }
	  //Deleting the used references
	  delete r;
	  delete hs;
 	    }
 	  }
 	  pixelColor /= (n*n);
 	  //pixelColor *= 2;
 	  imData[ny-j-1][i] = png::rgb_pixel(pixelColor[0]*255,pixelColor[1]*255,pixelColor[2]*255);
	} //end of inner for
    } //end of outer for
  //cout<<"Count:"<<count<<endl;
  //Writes the computed image data into a png image.
  if(sc->outputfile!="")
      imData.write(sc->outputfile);
  else
      imData.write( "new_image.png" );

} //end of computeRay

