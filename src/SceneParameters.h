/*
  File: SceneParameters.h
  
  Description: It holds all the images that are required for any environment.
  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013
*/

#ifndef SCENEPARAMETERS_H
#define SCENEPARAMETERS_H

#include <iostream>
#include "Vector3D.h"
#include "png++/png.hpp"

using namespace std;

namespace sivelab {
  
  class SceneParameters {
  public:
    //Scene* sc;
    Vector3D bgColor;
    png::image< png::rgb_pixel > posX;
    png::image< png::rgb_pixel > negX;
    png::image< png::rgb_pixel > posY;
    png::image< png::rgb_pixel > negY;
    png::image< png::rgb_pixel > posZ;
    png::image< png::rgb_pixel > negZ;
    
   // png::image< png::rgb_pixel > currentImage;
    
    SceneParameters();
    
    
  };
  
}

#endif