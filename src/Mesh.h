/*
  File: Mesh.h

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013
*/


#ifndef MESH_H
#define MESH_H

#include "Ray.h"
#include "Shape.h"
#include "Scene.h"
#include "Triangle.h"
//#include "Vector3D.h"
#include "HitStructure.h"
#include "BVHNode.h"

using namespace std;

namespace sivelab {

class Mesh: public Shape
{
 public:
  //double a,b,c,t1,t2,dis;
 Mesh(){}
 vector<Shape *> meshTriangles;
 BVHNode *meshRoot;
 void createBBox();
 void load(string&);
 bool intersect(Ray *,double&,double&,HitStructure *);
 
};
}

#endif
