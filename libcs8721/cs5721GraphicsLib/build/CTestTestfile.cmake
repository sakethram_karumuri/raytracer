# CMake generated Testfile for 
# Source directory: /home/csgrads/karum006/Desktop/libcs8721/cs5721GraphicsLib
# Build directory: /home/csgrads/karum006/Desktop/libcs8721/cs5721GraphicsLib/build
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(src)
SUBDIRS(tests)
SUBDIRS(utests)
