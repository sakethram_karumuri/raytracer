/*
  File: RenderObject.cpp

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013

Description: This class holds few important functs that are respponsible for finding the intersection of rays with the shapes. It also has few functions that are requires for computing the shaders.
*/


#include "RenderObject.h"
#include <math.h>
#include "Sphere.h"
#include "Triangle.h"
//#include "HitStructure.h"

using namespace sivelab;


RenderObject::RenderObject() {
  // cout<<"RenderObject created"<<endl;
}

//This function checks if the given ray intersects with any of the objects in the scene
int RenderObject::intersect(Ray *r,double& tmin,double& tmax, HitStructure *hs) {

  sc=Scene::getInstance();
  bool hit;

  if(sc->bvh=="no") {
  for(int i=0; i<sc->shapeObjects.size();i++)
    {
        hit = sc->shapeObjects[i]->intersect(r,tmin,tmax,hs);
	sc->tempIterator=i;
	if(hit)
	  sc->firstObj=i;
	else
	  continue;
	      
    }
}
    
   else { 
    hit = sc->root->intersect(r,tmin,tmax,hs);
   }
   
    return hit;

}

//This function returns a boolean value based on the intersection of the shadow ray with objects in the scene file.
bool RenderObject::intersectShadow(Ray *r,double& tmin,double& tmax, HitStructure *hs) {
 
  sc=Scene::getInstance();
  bool hit;
 
  if(sc->bvh=="no") {
  for(int i=0; i<sc->shapeObjects.size();i++)
    {
      hit = sc->shapeObjects[i]->intersect(r,tmin,tmax,hs);
      if(hit)
	return true;
      else
	continue;	
    }
}
 
  else {
    hit = sc->root->intersect(r,tmin,tmax,hs);
  }
    return hit;
}

//This function applies shader at the point of contact on the first hit object.
void RenderObject::applyShader(Ray *r,HitStructure *hs, Vector3D camPosition) {

  Vector3D l_final;  
  sc=Scene::getInstance();
   //l_final = sc->shapeObjects[hs->firstObjref]->shaderRef->applyShading(r,1.0e-3,numeric_limits<double>::max(),hs,camPosition);
   l_final = hs->firstObj->shaderRef->applyShading(r,1.0e-3,numeric_limits<double>::max(),hs,camPosition);
       hs->capL = l_final.clamp(0,1);
   
}

