/*
  File: InstancedObject.h
 Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013

Description: It contains the variables that are used by all the objects.
*/

#ifndef INSTANCEDOBJECT_H
#define INSTANCEDOBJECT_H

#include<iostream>
#include "Vector3D.h"
#include "Ray.h"
#include "HitStructure.h"
#include "Matrix.h"
#include "Shape.h"
#include <algorithm>
#include "BoundingBox.h"

using namespace std;

namespace sivelab {
  
  class InstancedObject: public Shape
  {
  public:
    
    Matrix *m;
    InstancedObject(Matrix*);
    void createBBox();
    bool intersect(Ray *,double&,double&,HitStructure*);
  };
}

#endif