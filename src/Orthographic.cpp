/*
  File: Orthographic.cpp

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013

Description: This class has various constructors that are used for creating different shaders.
*/


#include<iostream>
#include "Orthographic.h"
#include "Scene.h"
#include "RenderObject.h"
#include "HitStructure.h"

using namespace sivelab;

Orthographic::Orthographic() {
  //cout<<"Shader object created"<<endl;

}

Orthographic::Orthographic(string ty,Vector3D pos, Vector3D viewdr, Vector3D lookatpt, float focalLn, float imgPW, double wid, double hei) {

  //Declaration of variables.
  type=ty;
  position=pos;
  viewDir=viewdr;
  lookatPoint=lookatpt;
  focalLength=focalLn;
  imagePlaneWidth=imgPW;
  pixwidth=wid;
  pixheight=hei;

  //Calculation of aspect ratio
  aspratio = (float)pixwidth/(float)pixheight;
  imagePlaneHeight=imagePlaneWidth/aspratio;

  basis = new UVWbasis(position,lookatpt,viewDir);
}

void Orthographic::computeRay(int nx,int ny,float wid,float hei) {
  //code for Orthographic camera
}