add_library (sive-rtutil
  ArgumentParsing.cpp ArgumentParsing.h
  handleGraphicsArgs.cpp handleGraphicsArgs.h
  model_obj.cpp model_obj.h
  Random.cpp Random.h
  SceneDataContainer.h
  Timer.cpp Timer.h
  Vector3D.cpp Vector3D.h
  XMLSceneParser.h XMLSceneParser.cpp
  Camera.h Camera.cpp
  Perspective.h Perspective.cpp
  Orthographic.h Orthographic.cpp
  Light.h Light.cpp
  LightPoint.h LightPoint.cpp
  LightArea.h LightArea.cpp
  Shader.h Shader.cpp
  Ray.h Ray.cpp
  UVWbasis.h UVWbasis.cpp
  Scene.h Scene.cpp
  png++/png.hpp
  Shape.h Shape.cpp
  RenderObject.h RenderObject.cpp
  Sphere.h Sphere.cpp
  Triangle.h Triangle.cpp
  HitStructure.h HitStructure.cpp
  Box.h Box.cpp
  Lambertian.h Lambertian.cpp
  Blinnphong.h Blinnphong.cpp
  Mirror.h Mirror.cpp
  Glaze.h Glaze.cpp
  BlinnphongMirrored.h BlinnphongMirrored.cpp
  Texture.h Texture.cpp
  SceneParameters.h SceneParameters.cpp
  Matrix.h Matrix.cpp
  InstancedObject.h InstancedObject.cpp
  Mesh.h Mesh.cpp
  BoundingBox.h BoundingBox.cpp
  BVHNode.h BVHNode.cpp
  )

