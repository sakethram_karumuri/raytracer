/*
  File: Sphere.cpp

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013

Description: This class has intersection function for a sphere. Its checks the roots with tmax values and assigns tmax accordingly.
*/


#include "Sphere.h"

using namespace sivelab;

Sphere::Sphere() {
}

void Sphere::createBBox() {

   Vector3D minPoint,maxPoint,center_bb;
   
   minPoint[0] = center[0]-radius;
   minPoint[1] = center[1]-radius;
   minPoint[2] = center[2]-radius;

   maxPoint[0] = center[0]+radius;
   maxPoint[1] = center[1]+radius;
   maxPoint[2] = center[2]+radius;
   
  
   //cout<<"Min point: "<<minPoint<<" Max point: "<<maxPoint<<endl;
   bb = new BoundingBox(minPoint,maxPoint);
}

bool Sphere::intersect(Ray *r, double& tmin, double& tmax, HitStructure *hs) {


 
  Scene *sc=Scene::getInstance();
  a=(r->direction).dot(r->direction);
  b=2*(r->direction).dot(r->origin-center);
  c=(r->origin-center).dot(r->origin-center)-(radius*radius);
  
  dis=b*b-(4*a*c);

  if(dis<0) //if there is no intersection return false
    return false;
  
  else {    // if there is an intersection
    t1 = ((-1)*b)-sqrt(dis);
    t1/= 2*a;

    t2 = ((-1)*b)+sqrt(dis);
    t2/= 2*a;

    if(t1<t2){
      if(t1<tmax && t1>=tmin){
	if(r->type != "shadow"){ // if the ray is a shadow ray, dont change the tmax value
	tmax=t1;
	hs->tfinal=tmax;
	hs->ptContact=r->origin+ tmax*r->direction;
	hs->normal=hs->ptContact-center;
	hs->normal.normalize();
	hs->firstObj=this;
	}
	return true;
      }
    }
    else if (t2<t1) {
      if(t2<tmax && t2>=tmin){
	if(r->type != "shadow"){ //if the ray is a shadow ray, dont change the tmax value
	tmax=t2;
	hs->ptContact=r->origin+ tmax*r->direction;
	hs->normal=hs->ptContact-center;
	hs->normal.normalize();
	}
	return true;
      }
    }


    //cout<<"Ray origin:"<<r->origin<<"\tRay direction:"<<r->direction;
    //cout<<"	a:"<<a<<"\tb:"<<b<<"\tc:"<<c;
    //cout<<"	t1:"<<t1<<" & t2:"<<t2<<endl;
    return false;
  }
      
   

}
