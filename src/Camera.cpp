/*
  File: Camera.cpp

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013

  Description: This class is one of the main classes in the Ray tracer project. It is responsible for generating the rays for each pixel on the imageplane. It also holds a number of references that are further used in the project.
*/


#include<iostream>
#include "Camera.h"
#include "Scene.h"
#include "RenderObject.h"
#include "HitStructure.h"

using namespace sivelab;

Camera::Camera() {
  //std::cout<<"Default camera object created"<<std::endl;
  //UVWbasis *basis = new UVWbasis();
}
/*
 
Camera::Camera(string ty,Vector3D pos, Vector3D viewdr, Vector3D lookatpt, float focalLn, float imgPW, double wid, double hei) {

  //Declaration of variables.
  type=ty;
  position=pos;
  viewDir=viewdr;
  lookatPoint=lookatpt;
  focalLength=focalLn;
  imagePlaneWidth=imgPW;
  pixwidth=wid;
  pixheight=hei;

  //Calculation of aspect ratio
  aspratio = (float)pixwidth/(float)pixheight;
  imagePlaneHeight=imagePlaneWidth/aspratio;

  basis = new UVWbasis(position,lookatpt,viewDir);
}

//This function generates rays for each pixel in the image plane.
void Camera::computeRay(int nx,int ny,float wid,float hei)
{
  double l=(-wid)/2.0;
  double r=wid/2.0;
  double t=hei/2.0;
  double b=(-hei)/2.0;
  double tu,tv;
  double red,green,blue;
  bool hit=false;
  int count=0; //Tells how many pixels are hit in the final image.
  int firstObj; //Holds the reference of the object that was first hit.
  Vector3D bgColor;
  
  png::image< png::rgb_pixel > imData( nx, ny );
  RenderObject *ro = new RenderObject();
  Scene *sc = Scene::getInstance();
  //For each pixel
  for(size_t j=0;j<ny;j++)
    {
      for(size_t i=0;i<nx;i++)
	{
	  tu=l+(r-l)*(i+0.5)/(float)nx;
	  tv=b+(t-b)*(j+0.5)/(float)ny;

	  HitStructure *hs= new HitStructure();
	  //For perspective camera
	  if(type.compare("perspective")==0)
	    {
	      Ray *r= new Ray();
	      r->origin = position;
	      r->direction = (-1)*focalLength*basis->w + tu*basis->u + tv*basis->v;
// 	      sc->tmax=numeric_limits<double>::max();
// 	      sc->tmin=1.0;
	      sc->depth = 8;
	      double tmin = 1.0;
	      double tmax = numeric_limits<double>::max();
	      
	      firstObj = ro->intersect(r,tmin,tmax,hs);

	      if(tmax < numeric_limits<double>::max()){

		//hs->ptContact=r->origin+ tmax*r->direction;
		//position = hs->ptContact;
		hs->firstObjref=firstObj;
		//cout<<"Tmax value before shader is:"<<sc->tmax<<endl;
		
		ro->applyShader(r,hs,position);
		//cout<<"Tmax value after shader is:"<<sc->tmax<<endl;
		count++;
		
		imData[ny-j-1][i] = png::rgb_pixel(hs->capL[0]*255,hs->capL[1]*255,hs->capL[2]*255);

		//imData[ny-j-1][i] = png::rgb_pixel(red,green,blue);
	      }
	      else {
		//bgColor = sc->bgColor;
		bgColor = sc->enviMapping(r);
		imData[ny-j-1][i] = png::rgb_pixel(bgColor[0]*255,bgColor[1]*255,bgColor[2]*255);
	      }
	      //Deleting the used references
	      delete r;
	      delete hs;
	    } //end of if

	  //for orthographic camera
	  else if(type.compare("orthographic")==0)
	    {
	      Ray *r=new Ray();
	      r->origin = (-1)*basis->w;
	      r->direction = position + tu*basis->u + tv*basis->v;
	    } //end of else if
	} //end of inner for
    } //end of outer for
  cout<<"Count:"<<count<<endl;
  //Writes the computed image data into a png image.
  if(sc->outputfile!="")
      imData.write(sc->outputfile);
  else
      imData.write( "new_image.png" );

} //end of computeRay

*/