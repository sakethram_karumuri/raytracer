/*
 *  test_XMLSceneParse.cpp

Instructor: Pete Willemsen
Student: Sakethram Karumuri
Course: CS 8721
Prog Assignment: 1
Date: 10/11/2013

Description: This file does the XML Scene Parsing. It looks for different components like Shapes, Shaders, Lights, etc.. scans and pushes them accordingly into an vector present in the Scene class.

 *
 *  Created by Pete Willemsen on 10/6/09.
 *  Copyright 2009 Department of Computer Science, University of Minnesota-Duluth. All rights reserved.
 *
 * This file is part of CS5721 Computer Graphics library (cs5721Graphics).
 *
 * cs5721Graphics is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cs5721Graphics is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cs5721Graphics.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <iostream>
#include <sstream>
#include <cstdlib>
#include <cassert>

#include "Vector3D.h"
#include "XMLSceneParser.h"
#include "handleGraphicsArgs.h"
#include "Camera.h"
#include "Perspective.h"
#include "Orthographic.h"
#include "Light.h"
#include "Shader.h"
#include "Lambertian.h"
#include "Blinnphong.h"
#include "Ray.h"
#include "png++/png.hpp"
#include <cstdlib>
#include "Scene.h"
#include "Triangle.h"
#include "Sphere.h"
#include "Shape.h"
#include "InstancedObject.h"
#include <time.h>
#include "Box.h"
#include "Mesh.h"
#include "Texture.h"
#include "SceneParameters.h"
#include "Matrix.h"
#include "LightPoint.h"
#include "LightArea.h"
#include <math.h>

using namespace sivelab;

// Here's an example

double pixwidth,pixheight;
string shaderRef,diffuseRef,specularRef,enviMap; // Stores the shader reference to each shape object
bool isShader_instance;

class CameraCreator : public SceneElementCreator
{
public:
  CameraCreator() {}
  ~CameraCreator() {}
 
  void instance( ptree::value_type const &v )
  {
    std::istringstream buf;

    std::string name, type;
    Vector3D position, viewDir, lookatPoint, sample;
    bool lookatSet = false;
    float focalLength;
    float imagePlaneWidth;

    name = v.second.get<std::string>("<xmlattr>.name");
    type = v.second.get<std::string>("<xmlattr>.type");

    buf.str( v.second.get<std::string>("position") );
    buf >> position;
    buf.clear();

    // LookAtPoint and ViewDir are optional, but one should be specified.
    boost::optional<std::string> plookatPoint = v.second.get_optional<std::string>("lookatPoint");
    boost::optional<std::string > pviewDir = v.second.get_optional<std::string>("viewDir");

    if (plookatPoint) {
      lookatSet = true;
      buf.str( *plookatPoint );
      buf >> lookatPoint; 
      buf.clear();
    } else if (pviewDir) {
      buf.str( *pviewDir );
      buf >> viewDir; 
      buf.clear();
    }

    buf.str( v.second.get<std::string>("focalLength") );
    buf >> focalLength;
    buf.clear();

    buf.str( v.second.get<std::string>("imagePlaneWidth") );
    buf >> imagePlaneWidth;
    buf.clear();
    Camera *cam;
    if(type=="perspective")
      cam= new Perspective(type,position, viewDir, lookatPoint, focalLength, imagePlaneWidth, pixwidth, pixheight);
    else if(type=="orthographic")
      cam = new Orthographic(type,position, viewDir, lookatPoint, focalLength, imagePlaneWidth, pixwidth, pixheight);
   
    Scene *sc= Scene::getInstance();
    sc->c=cam;

  }

private:
};

class ContainerClass : public SceneElementCreator
{
public:
  ContainerClass() {}
  ~ContainerClass() {}

  void instance( ptree::value_type const &v )
  {
    //
    // Light
    //
    
    
    
    if (v.first == "light") {
      // Do what is necessary to pull out the light...


      std::istringstream buf;

      std::string type;
      Vector3D position, intensity, l_normal;
      double l_width;
      double l_length;

      type = v.second.get<std::string>("<xmlattr>.type");

      buf.str(v.second.get<std::string>("position"));
      buf>>position;
      buf.clear();


      buf.str(v.second.get<std::string>("intensity"));
      buf>>intensity;
      buf.clear();


      boost::optional<std::string> pwidth = v.second.get_optional<std::string>("width");
      if(pwidth){
	buf.str(*pwidth);
      buf>>l_width;
      buf.clear();
      }

      boost::optional<std::string> plength = v.second.get_optional<std::string>("length");
      if(plength){
	buf.str(*plength);
      buf>>l_length;
      buf.clear();
      }
     
      boost::optional<std::string> normal = v.second.get_optional<std::string>("normal");
      if(normal){
	buf.str(*normal);
      buf>>l_normal;
      buf.clear();
      }
      /*
	buf.str(v.second.get<std::string>("width"));
	buf>>pwidth;
	buf.clear();

	buf.str(v.second.get<std::string>("length"));
	buf>>plength;
	buf.clear();
      */

      if(type =="point") {
      Light *l = new LightPoint(position, intensity, type);
      l->isArea = false;
        Scene *sc =Scene::getInstance();
      sc->lightObjects.push_back(l);
      }
      
      else if(type == "area") {
      
	Light *l = new LightArea(position, intensity, type,l_normal);
      
      if(pwidth && plength) {
	l->width = l_width;
	l->length = l_length;
	//l->normal = l_normal;
	l->area = l_width*l_length;
	l->isArea = true;
	l->position[0] = l->position[0] - (l_width/2);
	l->position[1] = l->position[1] - (l_length/2);
      }
      Scene *sc =Scene::getInstance();
      sc->lightObjects.push_back(l);
      }
      /*
	std::cout<<"Light:  type="<<type<<std::endl;
	std::cout<<"\tposition:"<<position<<std::endl;
	std::cout<<"\tintensity:"<<intensity<<std::endl;
	if(pwidth)
	std::cout<<"\twidth:"<<pwidth<<std::endl;
	if(plength)
	std::cout<<"\tlength:"<<plength<<std::endl;
      */

    }


    // 
    // Shader
    // 
    if(v.first == "texture") {
      parseTextureData(v);
            
    }
    
    if (v.first == "shader") {
      parseShaderData(v);
    }

    // 
    // Shape or Instance
    // 
    if (v.first == "shape") {
      parseShapeData(v);
    }
  
  if(v.first == "instance") {
    parseInstanceData(v);
   }
  
    if (v.first == "sceneParameters") {
      parseSceneParameters(v);
    }
  }

  void parseInstanceData(ptree::value_type const &v ) {
    std::string type, name;
    type = v.second.get<std::string>("<xmlattr>.type");
    name = v.second.get<std::string>("<xmlattr>.name");
    
    ptree::const_assoc_iterator it = v.second.find("shader");
    if( it != v.second.not_found() )
      {
	parseShaderData( *it );
      }
    std::istringstream buf; 
     if (type == "sphere") {
      float radius;
      sivelab::Vector3D center;

      buf.str( v.second.get<std::string>("center") );
      buf >> center;
      buf.clear();
      Shape *sp = new Sphere();
      sp->name=name;
      sp->type=type;
      sp->center=center;
      sp->ref=shaderRef;
      
      radius = v.second.get<float>("radius");
      sp->radius=radius;
      Scene *sc =Scene::getInstance();
      sp->shaderRef= sc->shaderMap.find(shaderRef)->second;
      //sc->baseObjects.push_back(sp);
      sc->baseObjMap.insert(pair<std::string,Shape *>(name,sp));
      sp->createBBox();
      
     }
     
     if (type == "box") {
      sivelab::Vector3D minPt, maxPt;

      buf.str( v.second.get<std::string>("minPt") );
      buf >> minPt;
      buf.clear();
      buf.str( v.second.get<std::string>("maxPt") );
      buf >> maxPt;
      buf.clear();
      Shape *sp = new Box();
      sp->name=name;
      sp->type=type;
      sp->minPt=minPt;
      sp->maxPt=maxPt;
      sp->ref=shaderRef;
      Scene *sc =Scene::getInstance();
      sp->shaderRef= sc->shaderMap.find(shaderRef)->second;
      sc->baseObjMap.insert(pair<std::string,Shape *>(name,sp));
      sp->generateBaseTriangles();
      sp->createBBox();
    }
    
    if (type =="mesh") {
	std::string filename;
       buf.str( v.second.get<std::string>("file") );
       buf >> filename;
       buf.clear();
       Shape *sp = new Mesh();
       sp->name=name;
       sp->type=type;
       sp->ref=shaderRef;
       Scene *sc =Scene::getInstance();
       sc->baseObjMap.insert(pair<std::string,Shape *>(name,sp));
       sp->shaderRef= sc->shaderMap.find(shaderRef)->second;
       sp->load(filename);
    }
    
  }
  
  void parseShapeData( ptree::value_type const &v )
  {
    std::string type, name, id;
    type = v.second.get<std::string>("<xmlattr>.type");
    name = v.second.get<std::string>("<xmlattr>.name");
 

    // Make sure to find the shader data objects; HAVE to include a
    // shader in this version; Need to find the shader in the second part
    // of the shape pair
    ptree::const_assoc_iterator it = v.second.find("shader");
    if( it != v.second.not_found() )
      {
	parseShaderData( *it );
	isShader_instance=true;
      }
      else {
	isShader_instance=false;
      }
    if (type == "instance") {
   
      id = v.second.get<std::string>("<xmlattr>.id");
      Matrix* trans;
    ptree::const_assoc_iterator it2 = v.second.find("transform");
    if( it2 != v.second.not_found() )
      {
	trans = parseTransformData( *it2 );
      }
      
    Shape *sp = new InstancedObject(trans);
    sp->name=name;
    sp->type=type;
    sp->id=id;      
    sp->ref=shaderRef;
    
    Scene *sc =Scene::getInstance();
    //sp->shaderRef= sc->shaderMap.find(shaderRef)->second;
    sp->baseRef= sc->baseObjMap.find(id)->second;
   
    if(!isShader_instance) 
	sp->shaderRef= sp->baseRef->shaderRef;
    else 
	sp->shaderRef= sc->shaderMap.find(shaderRef)->second;
    sc->shapeObjects.push_back(sp);
    //cout<<"Shader for "<<sp->name<<" is: "<<sp->shaderRef->name<<endl;
    
    if(sp->baseRef->type=="box") {
      sp->baseRef->generateInstanceTriangles(name,type,sp->shaderRef,trans);
    }
    
      
    }
    
    std::istringstream buf;
    
    if (type == "sphere") {
      float radius;
      sivelab::Vector3D center;

      buf.str( v.second.get<std::string>("center") );
      buf >> center;
      buf.clear();
      Shape *sp = new Sphere();
      sp->name=name;
      sp->type=type;
      sp->center=center;
      sp->ref=shaderRef;
      radius = v.second.get<float>("radius");
      sp->radius=radius;
    

      //cout<<sp->ref<<endl;
      Scene *sc =Scene::getInstance();
      sp->shaderRef= sc->shaderMap.find(shaderRef)->second;
      sc->shapeObjects.push_back(sp);

    }

    if (type == "box") {
      sivelab::Vector3D minPt, maxPt;

      buf.str( v.second.get<std::string>("minPt") );
      buf >> minPt;
      buf.clear();
      Shape *sp = new Box();
      sp->name=name;
      sp->type=type;
      sp->minPt=minPt;
      sp->ref=shaderRef;
      buf.str( v.second.get<std::string>("maxPt") );
      buf >> maxPt;
      buf.clear();
      sp->maxPt=maxPt;
      //cout<<"Min point and Max point are:"<<sp->minPt<<" "<<sp->maxPt<<endl;
    
      Scene *sc =Scene::getInstance();
      sp->shaderRef = sc->shaderMap.find(shaderRef)->second;
      sp->generateTriangles();
       //cout<<"Shader ref inside box is: "<<sp->shaderRef->name<<endl;
      sc->shapeObjects.push_back(sp);

      //Box *box = new Box(name,shaderRef,minPt,maxPt);

    }

    if (type == "triangle") {
      sivelab::Vector3D v0, v1, v2;
      
      buf.str( v.second.get<std::string>("v0") );
      buf >> v0;
      buf.clear();
      Shape *sp = new Triangle();
      sp->name=name;
      sp->type=type;
      sp->ref=shaderRef;
      sp->v0=v0;

      buf.str( v.second.get<std::string>("v1") );
      buf >> v1;
      buf.clear();
      sp->v1=v1;

      buf.str( v.second.get<std::string>("v2") );
      buf >> v2;
      buf.clear();
      sp->v2=v2;

      Scene *sc =Scene::getInstance();
      sp->shaderRef = sc->shaderMap.find(shaderRef)->second;
      sc->shapeObjects.push_back(sp);
    }
  }

  void parseShaderData( ptree::value_type const &v )
  {
    std::istringstream buf;

    std::string type, name;
    
    
    
    boost::optional<std::string> optShaderRef = v.second.get_optional<std::string>("<xmlattr>.ref");
    if(optShaderRef)
      shaderRef=*optShaderRef;
    if (!optShaderRef) {
      type = v.second.get<std::string>("<xmlattr>.type");
      name = v.second.get<std::string>("<xmlattr>.name");
    }
    
      ptree::const_assoc_iterator it2 = v.second.find("diffuse");
      if( it2 != v.second.not_found() )
      {
	parseDiffuseData( *it2 );
      }
    
      ptree::const_assoc_iterator it3 = v.second.find("specular");
      if( it3 != v.second.not_found() )
      {
	parseSpecularData( *it3 );
      }
    // else 
    // std::cout << "Found optional shader ref: " << *optShaderRef << std::endl;

    // if name exists in the shader map and this is a ref, return the shader pointer
    // otherwise, add the shader if it NOT a ref and the name doesn't exist
    // final is to throw error
    if (!optShaderRef) {

      // Did not find the shader and it was not a reference, so create and return
      
      if (type == "Lambertian") {
	sivelab::Vector3D kd;


    	buf.str( v.second.get<std::string>("diffuse") );
	buf >> kd;
	buf.clear();
	//cout<<"Diffuse coef is:"<<kd<<endl;
	Shader *s=new Lambertian(name, type, kd,diffuseRef);
	diffuseRef="";
	//s->diffuseRef = diffuseRef;
	
	Scene *sc =Scene::getInstance();
	//sc->shaderObjects.push_back(s);
	//cout<<"Shader map size: "<<sc->shaderMap.size()<<endl;
	sc->shaderMap.insert(pair<std::string,Shader *>(name,s));
	//cout<<"Shader map size: "<<sc->shaderMap.size()<<endl;
	

      }
      else if (type == "BlinnPhong" || type == "Phong") {
	sivelab::Vector3D kd;
	sivelab::Vector3D ks;
	float phongExp;

	buf.str( v.second.get<std::string>("diffuse") );
	buf >> kd;
	buf.clear();
	//cout<<"Diffuse coef is:"<<kd<<endl;
	//cout<<diffuseRef<<endl;

	buf.str( v.second.get<std::string>("specular") );
	buf >> ks;
	buf.clear();
	//cout<<"Specular coef is:"<<ks<<endl;
	//cout<<specularRef<<endl;

	phongExp = v.second.get<float>("phongExp");
	Shader *s=new Blinnphong(name, type, kd, ks, phongExp,diffuseRef,specularRef);
	Scene *sc =Scene::getInstance();
	//sc->shaderObjects.push_back(s);
	sc->shaderMap.insert(pair<std::string,Shader *>(name,s));
	
      }
      
      else if (type == "Mirror") {
    
	
	Shader *s=new Mirror(name, type);
	double roughness;
	 boost::optional<std::string> rough = v.second.get_optional<std::string>("roughness");
      if(rough) {
	buf.str(*rough);
      buf>>roughness;
      s->roughness = roughness;
      s->isRoughness = true;
      buf.clear();
      }
      else 
	s->isRoughness = false;
	Scene *sc =Scene::getInstance();
	//sc->shaderObjects.push_back(s);
	sc->shaderMap.insert(pair<std::string,Shader *>(name,s));
	//cout<<"Shader map size: "<<sc->shaderMap.size()<<endl;
	
      }
      
      else if (type == "Glaze") {
	sivelab::Vector3D kd;
	double mirrorCoef;
	
	buf.str( v.second.get<std::string>("diffuse") );
	buf >> kd;
	buf.clear();

	buf.str( v.second.get<std::string>("mirrorCoef") );
	buf >> mirrorCoef;
	buf.clear();
	
	Shader *s=new Glaze(name, type, kd, mirrorCoef);
	double roughness;
	 boost::optional<std::string> rough = v.second.get_optional<std::string>("roughness");
      if(rough) {
	buf.str(*rough);
      buf>>roughness;
      s->roughness = roughness;
      s->isRoughness = true;
      buf.clear();
      }
       else 
	s->isRoughness = false;
	
	s->diffuseRef = diffuseRef;
	
	Scene *sc =Scene::getInstance();
	//sc->shaderObjects.push_back(s);
	sc->shaderMap.insert(pair<std::string,Shader *>(name,s));
	//cout<<"Shader map size: "<<sc->shaderMap.size()<<endl;
	
      }
      
      else if (type == "BlinnPhongMirrored") {
	sivelab::Vector3D kd,ks;
	double mirrorCoef;
	float phongExp;
	
	buf.str( v.second.get<std::string>("diffuse") );
	buf >> kd;
	buf.clear();

	buf.str( v.second.get<std::string>("mirrorCoef") );
	buf >> mirrorCoef;
	buf.clear();

	buf.str( v.second.get<std::string>("specular") );
	buf >> ks;
	buf.clear();
	
	phongExp = v.second.get<float>("phongExp");
	
	Shader *s=new BlinnphongMirrored(name, type, kd,ks,phongExp, mirrorCoef);
	s->diffuseRef = diffuseRef;
	s->specularRef = specularRef;
	
	double roughness;
	 boost::optional<std::string> rough = v.second.get_optional<std::string>("roughness");
      if(rough) {
	buf.str(*rough);
      buf>>roughness;
      s->roughness = roughness;
      s->isRoughness = true;
      //cout<<roughness<<endl;
      buf.clear();
      }
       else 
	s->isRoughness = false;
	
	Scene *sc =Scene::getInstance();
	//sc->shaderObjects.push_back(s);
	sc->shaderMap.insert(pair<std::string,Shader *>(name,s));
	//cout<<"Shader map size: "<<sc->shaderMap.size()<<endl;
	
      }
    }
  }

  
   // Hint!! this function could be modified to return the matrix!
   // From stackoverflow:
   // http://stackoverflow.com/questions/4597048/boost-property-tree-iterators-how-to-handle-them

  Matrix* parseTransformData( ptree::value_type const &v )
  {
    std::string name;
    vector<Matrix *> matrixList;
    Matrix *transformMatrix = new Matrix(4,4);
    Matrix *temp = new Matrix(4,4);
    transformMatrix->makeIdentity();
    name = v.second.get<std::string>("<xmlattr>.name");

    //transformMatrix->printMatrix();
    //cout<<"Transform found"<<endl;
    
    std::istringstream buf;
    ptree::const_iterator nodeIterator;
    for (nodeIterator=v.second.begin(); nodeIterator!=v.second.end(); ++nodeIterator) {
      if (nodeIterator->first == "translate") {
	
	Vector3D trans;
	buf.str( nodeIterator->second.get_value<std::string>() );
	buf >> trans;
	buf.clear();
	//cout<<"Translate found: "<<trans<<endl;
	
	Matrix *transMat = new Matrix(4,4);
	//transMat->printMatrix();
	transMat->applyTranslation(trans);
	//transMat->printMatrix();
	//cout<<"Size of matrixList: "<<matrixList.size()<<endl;
	matrixList.push_back(transMat);
	//cout<<"Size of matrixList: "<<matrixList.size()<<endl;

	// At this point, trans contains the translation
      }
      else if (nodeIterator->first == "rotate") {

	double rot;
	buf.str( nodeIterator->second.get_value<std::string>() );
	buf >> rot;
	buf.clear();

	//cout<<"Rotation found: "<<rot<<endl;
	std::string axis = nodeIterator->second.get<std::string>("<xmlattr>.axis");
	if (axis == "X") {
	  // X rotation!
	  Matrix *rotX = new Matrix(4,4);
	  rotX->applyRotationX(rot);
	  //rotX->printMatrix();
	  matrixList.push_back(rotX);
	  //cout<<"Size of matrixList: "<<matrixList.size()<<endl;
	  
	} else if (axis == "Y") {
	  // Y rotation!
	  Matrix *rotY = new Matrix(4,4);
	  rotY->applyRotationY(rot);
	  matrixList.push_back(rotY);
	  //cout<<"Size of matrixList: "<<matrixList.size()<<endl;
	} else if (axis == "Z") {
	  // Z rotation!
	  Matrix *rotZ = new Matrix(4,4);
	  rotZ->applyRotationZ(rot);
	  matrixList.push_back(rotZ);
	  //cout<<"Size of matrixList: "<<matrixList.size()<<endl;
	}
      }
      else if (nodeIterator->first == "scale") {
	sivelab::Vector3D scale;
	buf.str( nodeIterator->second.get_value<std::string>() );
	buf >> scale;
	buf.clear();
	//cout<<"Scale found: "<<scale<<endl;
	Matrix *scaleMat = new Matrix(4,4);
	scaleMat->applyScaling(scale);
	matrixList.push_back(scaleMat);
	//cout<<"Size of matrixList: "<<matrixList.size()<<endl;
	
	// Scale!
      }
    }
 
    for(int i=0;i<matrixList.size();i++) {
      //matrixList[i]->printMatrix();
    
      temp->multiply(transformMatrix,matrixList[i]);
      transformMatrix->data=temp->data;
      //temp->initialize();
    //  */
    }
    //transformMatrix->printMatrix();
    Matrix *trans_inv = transformMatrix->inverse();
    //trans_inv->printMatrix();
    
    return trans_inv;
  }

  void parseDiffuseData( ptree::value_type const &v)
  {
    
    boost::optional<std::string> optDiffuseRef = v.second.get_optional<std::string>("<xmlattr>.tex");
    
    if(optDiffuseRef) {
      diffuseRef=*optDiffuseRef;
      
    }
    }
    
    
  void parseSpecularData( ptree::value_type const &v)
  {
    
    boost::optional<std::string> optSpecularRef = v.second.get_optional<std::string>("<xmlattr>.tex");
    
    if(optSpecularRef) {
      specularRef=*optSpecularRef;
      
    }
    }

  void parseTextureData( ptree::value_type const &v)
  {
    std::string type, name, sourceFile;
    std::istringstream buf;
    type = v.second.get<std::string>("<xmlattr>.type");
    name = v.second.get<std::string>("<xmlattr>.name");
  
      
    if (type== "image") {
      buf.str( v.second.get<std::string>("sourcefile") );
      buf >> sourceFile;
      buf.clear();
      png::image< png::rgb_pixel > image(sourceFile);
      
      //cout<<"Source file: "<<sourceFile<<endl;

      Texture *t =new Texture();
      t->sourceFile= sourceFile;
      t->name= name;
      t->type= type;
      t->image= image;
      Scene *sc =Scene::getInstance();
      sc->textureObjects.push_back(t);
  //sc->textureObjects[0]->image.write("output4.png");
    }
    
    if (type == "PerlinNoise") {
     
  png::image< png::rgb_pixel > imData( 1000, 500  );
  
  double temp;
  int k1=1000,k2=1000;
  double w=0.05,tp;
 for (size_t y = 0; y < imData.get_height(); ++y)
    {
      for (size_t x = 0; x < imData.get_width(); ++x)
	{
	 //tp = floor(drand48()+0.5)*255;
	   tp = (1 + sin(k1*(x+y) + turbulance(k2*x,k2*y))/w)/2;
	 // tp = noise(k1*x,k2*y);
	   //cout<<"tp: "<<tp<<endl;
	  imData[y][x] = png::rgb_pixel(tp,tp,tp);
	}
    }
    
    
 // imData.write( "textured_image.png" );
      Texture *t =new Texture();
      //t->sourceFile= sourceFile;
      t->name= name;
      t->type= type;
      t->image= imData;
      Scene *sc =Scene::getInstance();
      sc->textureObjects.push_back(t);
    }    
    
  }
  
  void parseSceneParameters(ptree::value_type const &v) {
  
    Vector3D bgColor;
    std::istringstream buf;
    Scene *sc =Scene::getInstance();
  
    
     buf.str( v.second.get<std::string>("bgColor") );
     buf >> bgColor;
     buf.clear();
     sc->bgColor = bgColor;
     //cout<<bgColor<<endl;
     
     
     boost::optional<std::string> sourceFile = v.second.get_optional<std::string>("envMapPrefix");
        
   if(sourceFile) {
    sc->enviMap = "yes"; 
    //cout<<"Envi map is enabled"<<endl;
     SceneParameters *sp = new SceneParameters();
               
     png::image< png::rgb_pixel > image1(*sourceFile+"negX.png");
     sp->negX = image1;
      png::image< png::rgb_pixel > image2(*sourceFile+"posX.png");
     sp->posX = image2;
      png::image< png::rgb_pixel > image3(*sourceFile+"negY.png");
     sp->negY = image3;
      png::image< png::rgb_pixel > image4(*sourceFile+"posY.png");
     sp->posY = image4;
      png::image< png::rgb_pixel > image5(*sourceFile+"negZ.png");
     sp->negZ = image5;
      png::image< png::rgb_pixel > image6(*sourceFile+"posZ.png");
     sp->posZ = image6;
     
    sc->sp = sp;
     //cout<<"bgImages pushed into scene"<<endl;
      //image6.write("posZ.png");
      //sp->posY.write("scposY.png");
  }
  
  else {
    //cout<<"Envi map is disabled"<<endl;
  }
  }
  
  ///////

double noise(double x,double y) {
 
 //random noise function found on internet
 //It returns a double value in the range -1 to 1
   int n=(int)x+(int)y*57;
   n=(n<<13)^n;
   int nn=(n*(n*n*60493+19990303)+1376312589)&0x7fffffff;
   double b = 1.0-((double)nn/1073741824.0);
   //cout<<"noise: "<<b<<endl;
   return b;
  
}

double turbulance(double x,double y) {
  double turbulance;
  for(int i=0;i<10;i++) {  
    turbulance += (noise(pow(2,i)*x,pow(2,i)*y))/pow(2,i); 
  }
  //cout<<"turbulance: "<<turbulance<<endl;
  return turbulance;
}
///////

private:

};


int main(int argc, char *argv[])
{
  GraphicsArgs args;
  args.process(argc, argv);

  XMLSceneParser xmlScene;
  pixwidth = args.width, pixheight = args.height;
  Scene *sc =Scene::getInstance();
  
  //this can redirect the output image into any user defined file name
  sc->outputfile = args.outputFileName;
  
  //this is used for anti aliasing
  sc->rpp = args.rpp;
  
  //this is used to run the program using BVH/without using BVH
  sc->bvh = args.splitMethod;
  //cout<<"rays per pixel: "<<sc->rpp<<endl;
  // if(diffuseRef!="no texture")
  // register object creation function with xmlScene
  xmlScene.registerCallback("camera", new CameraCreator);

  xmlScene.registerCallback("light", new ContainerClass);
  xmlScene.registerCallback("shader", new ContainerClass);
  xmlScene.registerCallback("instance",new ContainerClass);
  xmlScene.registerCallback("shape", new ContainerClass);
  xmlScene.registerCallback("texture", new ContainerClass);
 
  xmlScene.registerCallback("sceneParameters", new ContainerClass);
  
 
  if (args.inputFileName != "")
    xmlScene.parseFile( args.inputFileName );
  else
    {
      std::cerr << "Need input file!" << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //Create BBoxes for all shapes and base objects
    //I didn't put this part in the shapes constructer as this method seemed to be easier for me for debugging.
    for(int i=0;i<sc->shapeObjects.size();i++){
      sc->shapeObjects[i]->createBBox();
    }
    
    if(sc->bvh=="splitMethod" || sc->bvh=="yes") 
      sc->root=new BVHNode(sc->shapeObjects,0);
    
  Camera *c = sc->c;
  c->computeRay(c->pixwidth,c->pixheight,c->imagePlaneWidth,c->imagePlaneHeight);
  
  exit(EXIT_SUCCESS);
}

