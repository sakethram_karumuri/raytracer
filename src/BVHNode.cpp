/*
  File: BVHNode.cpp

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013

Description: It contains the left right children and also holds some functionality.
*/

#include "BVHNode.h"
using namespace sivelab;

bool sortFuncX(Shape *a,Shape* b) {
   return (a->bb->center_bb[0] < b->bb->center_bb[0]);
}
bool sortFuncY(Shape *a,Shape* b) {
   return (a->bb->center_bb[1] < b->bb->center_bb[1]);
}
bool sortFuncZ(Shape *a,Shape* b) {
   return (a->bb->center_bb[2] < b->bb->center_bb[2]);
}


BVHNode::BVHNode(vector<Shape *> a,int axis) {

  int n = a.size();
  
  if(n==1) {
    
    leftChild=a[0];
    rightChild=NULL;
    bb=a[0]->bb;
  }
  
  else if(n==2) {
    leftChild=a[0];
    rightChild=a[1];
    bb=combine(a[0]->bb,a[1]->bb);
  }
   else {
     if(axis==0)
      sort(a.begin(),a.end(),sortFuncX);
     else if(axis==1)
      sort(a.begin(),a.end(),sortFuncY);
     else if(axis==2)
      sort(a.begin(),a.end(),sortFuncZ);
     
     vector<Shape *> first,second;
     for(int i=0;i<(n/2);i++)
       first.push_back(a[i]);
     for(int j=(n/2);j<n;j++)
       second.push_back(a[j]);
     
    
     leftChild = new BVHNode(first,(axis+1)%3);
     rightChild = new BVHNode(second,(axis+2)%3);
     bb=combine(leftChild->bb,rightChild->bb);
   }
}

BoundingBox* BVHNode::combine(BoundingBox* left,BoundingBox* right) {

  BoundingBox* bb_combine;
  Vector3D minPt_combine,maxPt_combine;
  minPt_combine[0]=min(left->minPt[0],right->minPt[0]);
  minPt_combine[1]=min(left->minPt[1],right->minPt[1]);
  minPt_combine[2]=min(left->minPt[2],right->minPt[2]);
  
  maxPt_combine[0]=max(left->maxPt[0],right->maxPt[0]);
  maxPt_combine[1]=max(left->maxPt[1],right->maxPt[1]);
  maxPt_combine[2]=max(left->maxPt[2],right->maxPt[2]);
  
  bb_combine = new BoundingBox(minPt_combine,maxPt_combine);
  return bb_combine;
}


bool BVHNode::intersect(Ray *r, double& tmin, double& tmax, HitStructure *hs) {

  if(bb->hit(r)){
    HitStructure *l_hs = new HitStructure();
    HitStructure *r_hs = new HitStructure();
    
    bool left_hit,right_hit;
    left_hit= leftChild!=NULL && leftChild->intersect(r,tmin,tmax,hs);
    right_hit= rightChild!=NULL && rightChild->intersect(r,tmin,tmax,hs);

  if(left_hit && right_hit){
    //Not mandatory
     if(l_hs->tfinal<r_hs->tfinal)
       hs=l_hs;
     else
       hs=r_hs;
    //Not mandatory
    return true;
  }
  else if(left_hit) {
    hs=l_hs;
    return true;
  }
  else if(right_hit) {
    hs=r_hs;
    return true;
  }
  else
    return false;
  
    
  }
  else
    return false;
  
return false;
}