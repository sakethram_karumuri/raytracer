/*
  File: Mirror.h

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013
*/


#ifndef MIRROR_H
#define MIRROR_H

#include<iostream>
#include "HitStructure.h"
#include "Ray.h"
#include "Vector3D.h"
#include "Scene.h"
#include "Shader.h"
#include "RenderObject.h"


using namespace std;

namespace sivelab {
  
  class Mirror: public Shader {
  public:
    //Ray *reflectionRay = new Ray();
    Scene *sc;
    //double tmax,tmin;
    //int depth;
    Vector3D temp;
    
    Mirror();  
    Mirror(string,string);
    
    Vector3D applyShading(Ray *,double,double,HitStructure *,Vector3D);
    
  };
  
  
}

#endif
