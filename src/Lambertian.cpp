/*
  File: Lambertian.cpp

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013

Description: This class applies Lambertian shader to the shape. The function applyLambertian returns the computed L value to the calling function.
*/


#include "Lambertian.h"

using namespace sivelab;

Lambertian::Lambertian() {
    
}

Lambertian::Lambertian(string nam,string ty,Vector3D diff,string diffRef) {
  name=nam;
  type=ty;
  diffuse=diff;
  diffuseRef=diffRef;
  //cout<<diffuseRef<<endl;
}

//Returns L value.
Vector3D Lambertian::applyShading(Ray *r,double tmin, double tmax, HitStructure *hs,Vector3D camPosition) {
  // j is the shader reference
  
  //cout<<"Diffuse in shape: "<<diffuse<<endl;
  sc=Scene::getInstance();
  Vector3D l,l_final;
  Vector3D kd;//,ks,h;
  double t,texel_s,texel_t;//,t2,t3,t4,t5;
  //float ph;
  double tempTmax;
  int isVisible=1; //this variable gives the information if shadowRay hits the light.
  bool shadowTemp = false;
  
  if(hs->firstObj->type=="sphere") {
     //cout<<texel_s<<" "<<texel_t<<endl;   
    sc->computeSTSphere(hs->ptContact,hs->firstObj,&texel_s,&texel_t);
    //cout<<texel_s<<" "<<texel_t<<endl;    
  }
  else if(hs->firstObj->type=="triangle") {
    sc->computeSTTriangle(hs->ptContact,hs->firstObj,&texel_s,&texel_t);
  }

  hs->view= camPosition - hs->ptContact;
  hs->view.normalize();
     
    
  for(int p=0;p<sc->lightObjects.size();p++) {
    hs->light= sc->lightObjects[p]->position - hs->ptContact;
    hs->light.normalize();
      
    //Shadows code
     
    shadowTemp= false;   
    double tmin_shadow = 1.0e-6;
    double tmax_shadow = 1.0;
    Ray *shadowRay = new Ray();
    shadowRay->type = "shadow";
    shadowRay->origin = hs->ptContact;
     
    Vector3D tempPosition;
      if(sc->lightObjects[p]->isArea) {
	//cout<<sc->lightObjects[p]->basis->u<<endl<<sc->lightObjects[p]->basis->v<<endl;
	tempPosition =  sc->lightObjects[p]->position + (sc->glossVector.back().x*sc->lightObjects[p]->basis->u*sc->lightObjects[p]->width) + (sc->glossVector.back().y*sc->lightObjects[p]->basis->v*sc->lightObjects[p]->length);
	
      }
      else
	tempPosition = sc->lightObjects[p]->position;
      //shadowRay->origin = hs->ptContact;
      shadowRay->direction = tempPosition - hs->ptContact;
   
    RenderObject *ro = new RenderObject();
      
    shadowTemp = ro->intersectShadow(shadowRay,tmin_shadow,tmax_shadow,hs);
    if(shadowTemp){
      isVisible=0;
      shadowTemp= false;
	
    }
    else { isVisible=1;}
    tmin_shadow=1.0;
    
    //Shadows code
  //cout<<diffuseRef<<endl;
    if(diffuseRef=="")
      kd = diffuse;
    else {
      //if the lambertian type is of perlin noise
      if(diffuseRef=="perlinNoise") 
     	kd=sc->perlinNoise(hs->ptContact);
      else {
	//cout<<diffuseRef<<endl;
	kd= sc->textureLookup(texel_s,texel_t,diffuseRef); 
      }
    }
    
    t = hs->normal.dot(hs->light);
    l = sc->lightObjects[p]->intensity;
    //applying formula  
    l_final = l_final + kd*l*max(0.0,t)*isVisible; 
    delete shadowRay,ro;

  }
  
  return l_final;
	
}
    
   
