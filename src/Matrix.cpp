/*
  File: Matrix.cpp

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013
*/

#include "Matrix.h"

using namespace sivelab;

Matrix::Matrix(int r, int c):data(r*c) {
  rows = r; columns = c;
  
  for(int i=0;i<rows;i++)
    for(int j=0;j<columns;j++)
      data[(i*columns)+j]=0;
      //(i*columns)+j;
     
}

Matrix::Matrix(Vector3D v,string type):data(4) {
  rows = 4;
  columns = 1;
    setValue(1,1,v[0]);
    setValue(2,1,v[1]);
    setValue(3,1,v[2]);
  if(type == "point") {
    setValue(4,1,1);
  }
  
  else if(type == "direction") {
    setValue(4,1,0);
  }
}

Vector3D Matrix::getVector3D() {
  Vector3D v;
  v[0]=data[0];
  v[1]=data[1];
  v[2]=data[2];
  return v;
}

Matrix::Matrix(vector<double> b):data(b.size()) {

  for(int i=0;i<data.size();i++)
    data[i]=b[i];
}

void Matrix::printMatrix() {

 // cout<<"Vector size:"<<data.size()<<endl;
   for(int i=0;i<rows;i++){
    for(int j=0;j<columns;j++)
      cout<<data[(i*columns)+j]<<"\t";
    cout<<endl;
  }
}

void Matrix::setValue(int r, int c,double value) {
  data[((r-1)*columns)+(c-1)]=value;
}

double Matrix::getValue(int r,int c) {
  return data[((r-1)*columns)+(c-1)];
}

void Matrix::makeIdentity() {

  for(int i=0;i<rows;i++){
    for(int j=0;j<columns;j++)
      if(i==j)
      data[(i*columns)+j]=1;
      else
      data[(i*columns)+j]=0;
    }
}

void Matrix::initialize() {
for(int i=0;i<rows;i++){
    for(int j=0;j<columns;j++)
        data[(i*columns)+j]=0;
}
}


void Matrix::multiply(Matrix* m1,Matrix* m2) {

  //if(columns==m2->rows){
  //Matrix *prod = new Matrix(rows,m2->columns);
  //cout<<"Size of resultant matrix is:"<<prod->data.size()<<endl;
  //prod->printMatrix();
  //}
  //else
  if(m1->columns==m2->rows) {
  for(int i=0;i<m1->rows;i++){
    for(int j=0;j<m2->columns;j++) {
      data[(i*columns)+j] = 0;
      for(int k=0;k<m1->columns;k++)
	data[(i*columns)+j] = data[(i*columns)+j] + (m1->data[(i*m1->columns)+k]*m2->data[(k*m2->columns)+j]);
    }
  }
  }
  
  else
    cout<<"Multiplication not possible"<<endl;
  //return prod;
  
}

Matrix* Matrix::transpose() {

  Matrix *trans = new Matrix(columns,rows);
  for(int i=0;i<rows;i++)
    for(int j=0;j<columns;j++)
      trans->data[(j*rows)+i] = data[(i*columns)+j];
  
  return trans;
   
}

void Matrix::applyTranslation(Vector3D trans) {
/*
  data[columns-1]=trans[0];
  data[(2*columns)-1]=trans[1];
  data[(3*columns)-1]=trans[2];
  data[(4*columns)-1]=1;
  */
setValue(1,1,1);
setValue(2,2,1);
setValue(3,3,1);
setValue(1,4,trans[0]);
setValue(2,4,trans[1]);
setValue(3,4,trans[2]);
setValue(4,4,1);
}

void Matrix::applyRotationX(double x) {

 setValue(1,1,1);
 setValue(2,2,cos(x*pi/180));
 setValue(2,3,-1*(sin(x*pi/180)));
 setValue(3,2,sin(x*pi/180));
 setValue(3,3,cos(x*pi/180));
 setValue(4,4,1);
}

void Matrix::applyRotationY(double x) {

 setValue(1,1,cos(x*pi/180));
 setValue(1,3,sin(x*pi/180));
 setValue(2,2,1);
 setValue(3,1,-1*(sin(x*pi/180)));
 setValue(3,3,cos(x*pi/180));
 setValue(4,4,1);
}

void Matrix::applyRotationZ(double x) {

 setValue(1,1,cos(x*pi/180));
 setValue(1,2,-1*(sin(x*pi/180)));
 setValue(2,1,sin(x*pi/180));
 setValue(2,2,cos(x*pi/180));
 setValue(3,3,1);
 setValue(4,4,1);
}

void Matrix::applyScaling(Vector3D scale) {

  setValue(1,1,scale[0]);
  setValue(2,2,scale[1]);
  setValue(3,3,scale[2]);
  setValue(4,4,1);
}
//Used a reference for finding the determinant of an nxn matrix recursively.
//Link: http://www.itmindia.edu/images/ITM/pdf/Determinant%20of%20nth%20order%20matrix.pdf
double Matrix::determinant() {
  
  if(rows==columns) {
    double det = findDet(data,rows);
    return det;
  }
  
  else
    return 0;
}

double Matrix::findDet(vector<double> b,int m) {

  int i,j;
  double sum = 0;
  vector<double> c(rows*columns);
  if(m==2){
    sum = (b[0]*b[columns+1])-(b[1]*b[columns]);
    return sum;
   }
   
  for(int p=0;p<m;p++) {
    int h=0,k=0;
    for(i=1;i<m;i++) {
      for(j=0;j<m;j++) {
	if(j==p)
	  continue;
	c[(h*columns)+k] = b[(i*columns)+j];
	k++;
	if(k==m-1) {
	  h++;
	  k=0;
	}
      }
    }
    sum = sum+b[p]*pow(-1,p)*findDet(c,m-1);
  }
  return sum;
}

Matrix* Matrix::inverse() {
  Matrix *inv = new Matrix(rows,columns);
  if(findDet(data,rows)==0) {
    cout<<"Inverse not possible"<<endl;
    inv->invPossible=false;
    }
  else {
    inv->invPossible=true;
    det=findDet(data,rows);
   // cout<<"The determinant is:"<<det<<endl;
    vector<double> coFac=findCofac(data,rows);
   /*
    cout<<"The cofactor matrix is:"<<endl;
    for(int i=0;i<rows;i++){
    for(int j=0;j<columns;j++)
      cout<<coFac[(i*columns)+j]<<"\t";
    cout<<endl;
  }
   */

    vector<double> adjMat(rows*columns);
    for(int i=0;i<rows;i++)
    for(int j=0;j<columns;j++)
      adjMat[(j*rows)+i] = coFac[(i*columns)+j];
    /*
    cout<<"The adjoint matrix is:"<<endl;
    for(int i=0;i<rows;i++){
    for(int j=0;j<columns;j++)
      cout<<adjMat[(i*columns)+j]<<"\t";
    cout<<endl;
  }
    */
    for(int i=0;i<rows;i++)
    for(int j=0;j<columns;j++)
      adjMat[(j*rows)+i] /= det;
    
   /* cout<<"The inverse matrix is:"<<endl;
    for(int i=0;i<rows;i++){
    for(int j=0;j<columns;j++)
      cout<<adjMat[(i*columns)+j]<<"\t";
    cout<<endl;
  }
  */
    inv->data=adjMat;
    //inv->printMatrix();
        
  }
  return inv; 
}

vector<double> Matrix::findCofac(vector<double> num,int f) {

  vector<double> b(rows*columns),fac(rows*columns);
  int p,q,m,n,i,j;
  
   for(q=0;q<f;q++) {
    for(p=0;p<f;p++) {
     m=0;n=0;
     for(i=0;i<f;i++) {
       for(j=0;j<f;j++) {
          if(i!= q && j!= p) {
            b[(m*columns)+n]=num[(i*columns)+j];
            if(n<(f-2))
             n++;
            else {
               n=0;
               m++;
               }
            }
        }
      }
      fac[(q*columns)+p]=pow(-1,q + p) * findDet(b,f-1);
    }
  }
  return fac;
}