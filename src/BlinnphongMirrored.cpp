/*
File: BlinnphongMirrored.cpp

Instructor: Pete Willemsen
Student: Sakethram Karumuri
Course: CS 8721
Prog Assignment: 1
Date: 10/11/2013

Description: This class applies Blingphong mirrored shader to the object.
*/


#include "BlinnphongMirrored.h"

using namespace sivelab;

BlinnphongMirrored::BlinnphongMirrored(){
}

BlinnphongMirrored::BlinnphongMirrored(string nam,string ty, Vector3D diff, Vector3D spec, float phEx, double mirr) {
  name = nam;
  type= ty;
  diffuse = diff;
  specular = spec;
  phExp = phEx;
  mirrorCoef = mirr;
  //cout<<"Diffuse is: "<<diffuse<<" Passed is: "<<diff<<endl;
}

Vector3D BlinnphongMirrored::applyShading(Ray *r,double tmin, double tmax, HitStructure *hs,Vector3D camPosition) {
  
  Vector3D l_final,l_blinn;
  sc=Scene::getInstance();
  
  Blinnphong *bph = new Blinnphong(name,type,diffuse,specular,phExp,diffuseRef,specularRef);
  l_blinn = bph->applyShading(r,1.0e-3,numeric_limits<double>::max(),hs,camPosition);
  l_blinn = (1-mirrorCoef)*l_blinn;
  
  //cout<<"l_blinn:"<<l_blinn<<endl;
  
//Creating reflection ray
  Ray *refRay = new Ray();
  bool firstObj;
  //kd= sc->shaderObjects[j]->diffuse;
 // mirrCoef = sc->shaderObjects[j]->mirrorCoef;
  HitStructure *hsMirror = new HitStructure();
  
  refRay->type = "reflection";
  refRay->origin = hs->ptContact;
  hs->view= camPosition - hs->ptContact;
  hs->view.normalize();
  

  refRay->direction = (-1)*hs->view + 2*(hs->view.dot(hs->normal))*hs->normal;
  //cout<<isRoughness<<endl;

   if(isRoughness) {
    double u,v;
    UVWbasis *basis = new UVWbasis(hs->ptContact,refRay->direction);
    Vec2D tempVec = sc->glossVector.back();
    u = (roughness/-2) + tempVec.x*roughness;
    v = (roughness/-2) + tempVec.y*roughness;
    refRay->direction = refRay->direction + (u*basis->u) + (v*basis->v);
    delete basis;
  }
  
  
  double tmin_reflection = 1.0e-03;
  double tmax_reflection = numeric_limits<double>::max();
  RenderObject *ro = new RenderObject();

  firstObj = ro->intersect(refRay,tmin_reflection,tmax_reflection,hsMirror);

 // If the reflected ray doesnt hit anything, return the backgroud color. 
  if(!firstObj){
     l_final = sc->enviMapping(refRay);
     l_blinn+= l_final;
    //return l_final;
    return l_blinn;
  }
  else {
    hsMirror->firstObjref=firstObj;
    camPosition = hs->ptContact;
    ro->applyShader(refRay,hsMirror,camPosition);
    l_final.set(1,1,1);
    //return l_final;
    //cout<<"l_mirr:"<<mirrCoef*hsMirror->capL<<endl<<endl;
    l_blinn+= mirrorCoef*hsMirror->capL;
    //return mirrCoef*hsMirror->capL;
    return l_blinn;
  }
  
  
  
}
