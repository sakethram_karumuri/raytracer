/*
  File: InstancedObject.cpp

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013

Description: This class has intersection function for a sphere. Its checks the roots with tmax values and assigns tmax accordingly.
*/

#include "InstancedObject.h"

using namespace sivelab;

InstancedObject::InstancedObject(Matrix* trans) {
  m=trans;
}

void InstancedObject::createBBox() {

  
  Vector3D minPoint,maxPoint;
  minPoint = baseRef->bb->minPt;
  maxPoint = baseRef->bb->maxPt;
  
  //cout<<"Base min and maxpoint are:"<<minPoint<<" "<<maxPoint<<endl;
  Vector3D a1,b1,c1,d1,e1,f1,g1,h1;
  a1=minPoint;
  b1.set(maxPoint[0],minPoint[1],minPoint[2]);
  c1.set(maxPoint[0],minPoint[1],maxPoint[2]);
  d1.set(minPoint[0],minPoint[1],maxPoint[2]);
  e1.set(minPoint[0],maxPoint[1],maxPoint[2]);
  f1.set(minPoint[0],maxPoint[1],minPoint[2]);
  g1.set(maxPoint[0],maxPoint[1],minPoint[2]);
  h1=maxPoint;
  
  //m->printMatrix();
  
  Matrix* inv_inv = m->inverse();
  
  //inv_inv->printMatrix();
  //cout<<"a1: "<<a1<<endl;
  Matrix* mod_a1 = new Matrix(a1,"point");
  //mod_a1->printMatrix();
  Matrix* a1_mul = new Matrix(4,1);
  a1_mul->multiply(inv_inv,mod_a1);
  
  Matrix* mod_b1 = new Matrix(b1,"point");
  Matrix* b1_mul = new Matrix(4,1);
  b1_mul->multiply(inv_inv,mod_b1);
  
  Matrix* mod_c1 = new Matrix(c1,"point");
  Matrix* c1_mul = new Matrix(4,1);
  c1_mul->multiply(inv_inv,mod_c1);
  
  Matrix* mod_d1 = new Matrix(d1,"point");
  Matrix* d1_mul = new Matrix(4,1);
  d1_mul->multiply(inv_inv,mod_d1);
  
  Matrix* mod_e1 = new Matrix(e1,"point");
  Matrix* e1_mul = new Matrix(4,1);
  e1_mul->multiply(inv_inv,mod_e1);
  
  Matrix* mod_f1 = new Matrix(f1,"point");
  Matrix* f1_mul = new Matrix(4,1);
  f1_mul->multiply(inv_inv,mod_f1);
  
  Matrix* mod_g1 = new Matrix(g1,"point");
  Matrix* g1_mul = new Matrix(4,1);
  g1_mul->multiply(inv_inv,mod_g1);
  
  Matrix* mod_h1 = new Matrix(h1,"point");
  Matrix* h1_mul = new Matrix(4,1);
  h1_mul->multiply(inv_inv,mod_h1);
  
  Vector3D a1_mod,b1_mod,c1_mod,d1_mod,e1_mod,f1_mod,g1_mod,h1_mod;
  
  //vector<Vector3D> mod_vertexList;
 
  
  a1_mod = a1_mul->getVector3D();
  //mod_vertexList.push_back(a1_mod);
  b1_mod = b1_mul->getVector3D();
  //mod_vertexList.push_back(b1_mod);
  c1_mod = c1_mul->getVector3D();
  //mod_vertexList.push_back(c1_mod);
  d1_mod = d1_mul->getVector3D();
  //mod_vertexList.push_back(d1_mod);
  e1_mod = e1_mul->getVector3D();
  //mod_vertexList.push_back(e1_mod);
  f1_mod = f1_mul->getVector3D();
  //mod_vertexList.push_back(f1_mod);
  g1_mod = g1_mul->getVector3D();
  //mod_vertexList.push_back(g1_mod);
  h1_mod = h1_mul->getVector3D();
  //mod_vertexList.push_back(h1_mod);
  
  
  Vector3D minPoint_mod,maxPoint_mod;
  
  minPoint_mod[0] = min(a1_mod[0],min(b1_mod[0],min(c1_mod[0],min(d1_mod[0],min(e1_mod[0],min(f1_mod[0],min(g1_mod[0],h1_mod[0])))))));
  
  minPoint_mod[1] = min(a1_mod[1],min(b1_mod[1],min(c1_mod[1],min(d1_mod[1],min(e1_mod[1],min(f1_mod[1],min(g1_mod[1],h1_mod[1])))))));
  
  minPoint_mod[2] = min(a1_mod[2],min(b1_mod[2],min(c1_mod[2],min(d1_mod[2],min(e1_mod[2],min(f1_mod[2],min(g1_mod[2],h1_mod[2])))))));
  
  maxPoint_mod[0] = max(a1_mod[0],max(b1_mod[0],max(c1_mod[0],max(d1_mod[0],max(e1_mod[0],max(f1_mod[0],max(g1_mod[0],h1_mod[0])))))));
  
  maxPoint_mod[1] = max(a1_mod[1],max(b1_mod[1],max(c1_mod[1],max(d1_mod[1],max(e1_mod[1],max(f1_mod[1],max(g1_mod[1],h1_mod[1])))))));
  
  maxPoint_mod[2] = max(a1_mod[2],max(b1_mod[2],max(c1_mod[2],max(d1_mod[2],max(e1_mod[2],max(f1_mod[2],max(g1_mod[2],h1_mod[2])))))));
  
  bb = new BoundingBox(minPoint_mod,maxPoint_mod);
  
  
}

bool InstancedObject::intersect(Ray *r,double& tmin,double& tmax, HitStructure *hs) {
  Ray* trans = new Ray();
  Matrix* ray_ori = new Matrix(r->origin,"point");
  Matrix* ray_dir = new Matrix(r->direction,"direction");
  if(r->type=="shadow")
    trans->type="shadow";
  
  //cout<<"ray_ori matrix:"<<endl;
  //cout<<r->direction<<endl;
  //ray_dir->printMatrix();
  
  //m->printMatrix();
  Matrix *ray_mod_ori = new Matrix(4,1);
  ray_mod_ori->multiply(m,ray_ori);
  
  Matrix *ray_mod_dir = new Matrix(4,1);
  ray_mod_dir->multiply(m,ray_dir);
  trans->origin = ray_mod_ori->getVector3D();
  trans->direction = ray_mod_dir->getVector3D();
  
  //cout<<r->direction<<endl;
  //cout<<trans->origin<<endl<endl;
  bool hit = baseRef->intersect(trans,tmin,tmax,hs);
  
  if(hit) {
  if(r->type!="shadow") {
    hs->ptContact=r->origin+ tmax*r->direction;
 
    Matrix* normal = new Matrix(hs->normal,"direction");
    Matrix* normal_mod = new Matrix(4,1);
    Matrix* m_transpose = m->transpose();
    normal_mod->multiply(m_transpose,normal);
    hs->normal = normal_mod->getVector3D();
    hs->normal.normalize();
//cout<<"Normal inside instanced object is: "<<hs->normal<<endl;     
hs->firstObj=this;
   }
  }
  return hit;
  
}
