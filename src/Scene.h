/*
  File: Scene.h

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013

Description: This class is implemented using Singleton design pattern. Whenever an instance is created, it is made to point to the same memory location alla the time hence enabling a global access to the variables.
*/


#ifndef SCENE_H
#define SCENE_H

#include<iostream>
#include<vector>
#include "Camera.h"
#include "Shape.h"
#include "Shader.h"
#include "Light.h"
#include "Texture.h"
#include "png++/png.hpp"
#include "Vector3D.h"
#include<math.h>
#include "SceneParameters.h"
#include "Ray.h"
#include "BVHNode.h"

using namespace std;



namespace sivelab 
{

  class Scene {

  public:
    int a,b,tempIterator;
    //double tmax,tmin;
    int firstObj;
    int depth,axis,rpp;
    Vector3D kd,bgColor;
    vector<int> intObjects;
    vector<Shape *> shapeObjects;
    vector<Shape *> baseObjects;//holds different shapes like spheres, triangles.
    //vector<Shader *> shaderObjects; //holds different shaders like lambertian, blinnphong, mirror, blinphong mirrored.
    map <string,Shader *> shaderMap;
    map <string,Shape *> baseObjMap;
    vector<Light *> lightObjects; //holds all the lights in the scene file.
    vector<Texture *> textureObjects;
    Camera* c;
    SceneParameters* sp;
    png::image< png::rgb_pixel >* currentImage;
    string enviMap,outputfile,bvh;
    png::image< png::rgb_pixel > perlinImage;
    BVHNode *root;
    vector<Vec2D> glossVector;


    
    static Scene* getInstance(); // this method returns a pointer to the one and one created object.
    static Scene *single;
    
    Vector3D textureLookup(double,double,string);
    void computeSTSphere(Vector3D,Shape*,double*,double*);
    void computeSTTriangle(Vector3D,Shape*,double*,double*);
    Vector3D enviMapping(Ray*);
    double noise(double,double);
    Vector3D perlinNoise(Vector3D);
    double turbulance(double,double);
    
  private:
    Scene();

  };

}

#endif
