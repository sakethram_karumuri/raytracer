/*
  File: Glaze.h

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013
*/

#ifndef GLAZE_H
#define GLAZE_H

using namespace std;
#include <iostream>
#include "HitStructure.h"
#include "Ray.h"
#include "Vector3D.h"
#include "Scene.h"
#include "Shader.h"
#include "RenderObject.h"

namespace sivelab {
  class Glaze: public Shader {
  
  public:
    Scene *sc;
    
    double mirrCoef;
    Glaze();
    Glaze(string,string,Vector3D,double);
    
    Vector3D applyShading(Ray *,double,double,HitStructure *,Vector3D);
    
    
    
  };
  
}

#endif
