/*
  File: BoundingBox.h

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013

Description: It holds the shapes in it.
*/

#ifndef BOUNDINGBOX_H
#define BOUNDINGBOX_H

#include <iostream>
#include "Vector3D.h"
#include "Ray.h"


using namespace std;
namespace sivelab {

class BoundingBox {
public:
  
  Vector3D minPt,maxPt,center_bb;
  BoundingBox(){}
  BoundingBox(Vector3D,Vector3D);
  bool hit(Ray *);
};

}

#endif