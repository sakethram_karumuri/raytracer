/*
  File: BVHNode.h

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013

Description: It contains the left right children and also holds some functionality.
*/

#ifndef BVHNODE_H
#define BVHNODE_H

#include <iostream>
#include <vector>
#include <algorithm>
#include "Vector3D.h"
#include "Shape.h"
#include "HitStructure.h"
#include "Ray.h"
#include "BoundingBox.h"

using namespace std;
namespace sivelab {

  class BVHNode: public Shape {
  public:
    
    Shape *leftChild;
    Shape *rightChild;
    BVHNode(vector<Shape *>,int);
    bool intersect(Ray *,double&,double&,HitStructure *);
    BoundingBox* combine(BoundingBox*, BoundingBox*);
};
  
}
#endif