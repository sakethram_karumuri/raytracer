/*
  File: Matrix.h

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013
*/

#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>
#include "Vector3D.h"
#include <vector>
#define pi 3.1415

using namespace std;

namespace sivelab
{
  
  class Matrix {
  public:
    
    string type;
    int rows,columns;
    double det;
    bool invPossible;
    vector<double> data;
    //vector<double> temp;
    Matrix() {}
    Matrix(int,int);
    Matrix(vector<double>);
    Matrix(Vector3D,string);
    void printMatrix();
    void setValue(int,int,double);
    double getValue(int,int);
    void makeIdentity();
    void initialize();
    void multiply(Matrix*,Matrix*);
    Vector3D getVector3D();
    Matrix* transpose();
    void applyTranslation(Vector3D);
    void applyRotationX(double);
    void applyRotationY(double);
    void applyRotationZ(double);
    void applyScaling(Vector3D);
    double determinant();
    double findDet(vector<double>,int);
    vector<double> findCofac(vector<double>,int);
    Matrix* inverse();
  };
}

#endif