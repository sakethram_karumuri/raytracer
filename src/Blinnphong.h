/*
File: Blinnphong.h

Instructor: Pete Willemsen
Student: Sakethram Karumuri
Course: CS 8721
Prog Assignment: 1
Date: 10/11/2013
*/

#ifndef BLINNPHONG_H
#define BLINNPHONG_H

#include <iostream>
#include "HitStructure.h"
#include "Ray.h"
#include "Vector3D.h"
#include "Scene.h"
#include "RenderObject.h"

using namespace std;

namespace sivelab {

  class Blinnphong: public Shader {
  public:
    Scene *sc;
    
    Blinnphong();
    
    Blinnphong(string,string,Vector3D,Vector3D,float,string,string);
    
    Vector3D applyShading(Ray *,double,double,HitStructure *,Vector3D);
 
    
  };
  
}

#endif
