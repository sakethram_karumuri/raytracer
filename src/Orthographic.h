/*
File: Orthographic.h

Instructor: Pete Willemsen
Student: Sakethram Karumuri
Course: CS 8721
Prog Assignment: 1
Date: 10/11/2013
*/


#ifndef ORTHOGRAPHIC_H
#define ORTHOGRAPHIC_H

#include<iostream>
#include "Camera.h"
#include "Vector3D.h"
#include "UVWbasis.h"
#include "Ray.h"
#include "png++/png.hpp"
#include <cstdlib>
#include "handleGraphicsArgs.h"



using namespace std;


namespace sivelab
{

class Orthographic: public Camera {

public:

// Vector3D position, viewDir, lookatPoint;
// float focalLength, imagePlaneWidth, imagePlaneHeight;
// double pixwidth,pixheight,aspratio;
// string type;
// UVWbasis *basis;
//int nx,ny,width,height;


Orthographic();
Orthographic(string,Vector3D,Vector3D,Vector3D,float,float,double,double);

void computeRay(int,int, float, float);
};

}

#endif
