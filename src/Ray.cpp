/*
  File: Ray.cpp

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013

Description: Intializes the ray object with its origin and direction.
*/


#include<iostream>
#include "Ray.h"

using namespace sivelab;

Ray::Ray() {
  type= "normal";
  //cout<<"Ray object created"<<endl;
}

Ray::Ray(Vector3D vector) {
  tempRay=vector;
  lengthofRay(tempRay);
  //cout<<"Ray Details:\n"<<tempRay<<endl;
  //cout<<"Ray length:"<<lengthRay<<endl;
}

Ray::Ray(const double x,const double y,const double z) {
  tempRay.set(x,y,z);
  lengthRay = lengthofRay(tempRay);
  //cout<<"Ray Details:\n"<<tempRay<<endl;
  //cout<<"Ray length:"<<lengthRay<<endl;
}



double Ray::lengthofRay(Vector3D r) {
  return r.length();
}

