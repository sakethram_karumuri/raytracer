/*
  File: BoundingBox.cpp

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013

Description: It contains the variables that are used by all the objects.
*/

#include "BoundingBox.h"

using namespace sivelab;

BoundingBox::BoundingBox(Vector3D minPoint, Vector3D maxPoint) {
  minPt=minPoint;
  maxPt=maxPoint;
  center_bb = (minPt+maxPt)/2;
}

bool BoundingBox::hit(Ray *r) {

double xe,ye,ze,xd,yd,zd;
double a,b,c,txmin,txmax,tymin,tymax,tzmin,tzmax;

xe=r->origin[0]; ye=r->origin[1]; ze=r->origin[2];
xd=r->direction[0]; yd=r->direction[1]; zd=r->direction[2];

  a=1/xd; b=1/yd; c=1/zd;
  
  if(a>=0) {
    txmin = a*(minPt[0]-xe);
    txmax = a*(maxPt[0]-xe);
  }
  else {
    txmin = a*(maxPt[0]-xe);
    txmax = a*(minPt[0]-xe);
  }
  
  if(b>=0) {
    tymin = b*(minPt[1]-ye);
    tymax = b*(maxPt[1]-ye);
  }
  else {
    tymin = b*(maxPt[1]-ye);
    tymax = b*(minPt[1]-ye);
  }
   
  if(c>=0) {
    tzmin = c*(minPt[2]-ze);
    tzmax = c*(maxPt[2]-ze);
  }
  else {
    tzmin = c*(maxPt[2]-ze);
    tzmax = c*(minPt[2]-ze);
  }

  if(txmin>tymax || tymin>txmax || tzmin>tymax || tymin>tzmax || txmin>tzmax || tzmin>txmax)
    return false;
  else
    return true;
  
}