/*
  File: Shader.h

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013
*/


#ifndef SHADER_H
#define SHADER_H

#include<iostream>
#include "Vector3D.h"
#include "Ray.h"
#include "HitStructure.h"

using namespace std;

namespace sivelab
{

class Shader {

public:

string name,type;
string diffuseRef,specularRef;
Vector3D diffuse,specular;
float phExp;
double mirrorCoef,roughness;
bool isRoughness;

Shader();
Shader(string,string,Vector3D);
Shader(string,string,Vector3D,Vector3D,float);
Shader(string,string);
Shader(string,string,Vector3D,double);
Shader(string,string,Vector3D,Vector3D,float,double);

virtual Vector3D applyShading(Ray*,double,double, HitStructure*, Vector3D)=0;

};

}

#endif
