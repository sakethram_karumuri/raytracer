/*

File: Blinnphong.cpp

Instructor: Pete Willemsen
Student: Sakethram Karumuri
Course: CS 8721
Prog Assignment: 1
Date: 10/11/2013

Description: This class is used for applying Blinnphong shader to the shape.

*/

#include "Blinnphong.h"
#include<cmath>
using namespace sivelab;

Blinnphong::Blinnphong() {
}

Blinnphong::Blinnphong(string nam, string ty, Vector3D diff, Vector3D spec, float phongExp, string diffRef,string specRef) {
  //cout<<"Helloooooo"<<endl;
  name=nam;
  type=ty;
  diffuse=diff;
  specular=spec;
  phExp=phongExp;
  diffuseRef=diffRef;
  specularRef=specRef;
  
 // cout<<"Diffuse: "<<diff<<"Diffuse in shape: "<<diffuse<<endl;
}

Vector3D Blinnphong::applyShading(Ray *r,double tmin, double tmax, HitStructure *hs,Vector3D camPosition) {
// j is the shader reference

  //cout<<"Diffuse in shape: "<<diffuse<<endl;
//Declaration of variables
sc=Scene::getInstance();
  Vector3D l,l_final;
  Vector3D kd,ks,h;
  double t,t2,t3,t4,t5;
  double texel_s,texel_t;
  float ph;
  double tempTmax;
  int isVisible=1;
  bool shadowTemp = false;
  

  if(hs->firstObj->type=="sphere") {
    
     sc->computeSTSphere(hs->ptContact,hs->firstObj,&texel_s,&texel_t);
    
  }
  else if(hs->firstObj->type=="triangle") {
  
     sc->computeSTTriangle(hs->ptContact,hs->firstObj,&texel_s,&texel_t);
    
  }
    
     hs->view= camPosition - hs->ptContact;
     hs->view.normalize();

     // Compute the effect of all lights in getting the shadows at a point of intersection. 
     
    for(int p=0;p<sc->lightObjects.size();p++) {
      hs->light= sc->lightObjects[p]->position - hs->ptContact;
      hs->light.normalize();
      
      //Shadows code
      shadowTemp= false;
      double tmin_shadow = 1.0e-6;
      double tmax_shadow = 1.0;
      Ray *shadowRay = new Ray();
      shadowRay->type = "shadow";
      Vector3D tempPosition;
      if(sc->lightObjects[p]->isArea) {
	//cout<<sc->lightObjects[p]->basis->u<<endl<<sc->lightObjects[p]->basis->v<<endl;
	tempPosition =  sc->lightObjects[p]->position + (sc->glossVector.back().x*sc->lightObjects[p]->basis->u*sc->lightObjects[p]->width) + (sc->glossVector.back().y*sc->lightObjects[p]->basis->v*sc->lightObjects[p]->length);
	
      }
      else
	tempPosition = sc->lightObjects[p]->position;
      shadowRay->origin = hs->ptContact;
      shadowRay->direction = tempPosition - hs->ptContact;

      RenderObject *ro = new RenderObject();
      shadowTemp = ro->intersectShadow(shadowRay,tmin_shadow,tmax_shadow,hs);
      if(shadowTemp){
	isVisible=0;
	 shadowTemp= false;
      }
      else { isVisible=1;}
      delete shadowRay;
      tmin_shadow=1.0;
      //Shadows code
     
     if(diffuseRef=="")
	kd= diffuse;
    else {
       kd= sc->textureLookup(texel_s,texel_t,diffuseRef);
	//cout<<"Diffuse: "<<kd<<endl;
      }
      if(specularRef=="")
	ks= specular;
     else {
       ks= sc->textureLookup(texel_s,texel_t,specularRef);
	//cout<<"Specular: "<<ks<<endl<<endl;
      }
      t = hs->normal.dot(hs->light);
      l = sc->lightObjects[p]->intensity;
      hs->reflec = (-1)*l+ 2*(l.dot(hs->normal))*hs->normal;
      t2 = hs->view.dot(hs->reflec);
      t3 = max(0.0,t2);
      h= hs->view+ hs->light;
      h.normalize();
      t4 = hs->normal.dot(h);
      t5= max(0.0,t4);
      ph = phExp;
      l_final = l_final + (kd*l*max(0.0,t) + ks*l*pow(t5,ph))*isVisible;
    
  } // end of BlinnPhong

return l_final;
}
		
 
