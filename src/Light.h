/*
  File: Light.h

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013
*/

#ifndef LIGHT_H
#define LIGHT_H

#include<iostream>
#include "Vector3D.h"
#include "UVWbasis.h"

using namespace std;

namespace sivelab
{

  class Light {

  public:

    Vector3D position, intensity, normal;
    string type;
    bool isArea;
    double area,width,length;
    UVWbasis *basis;

    Light();
    virtual Vector3D getPosition()=0;
  };

}

#endif
