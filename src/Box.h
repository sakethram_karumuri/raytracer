/*
File: Box.h

Instructor: Pete Willemsen
Student: Sakethram Karumuri
Course: CS 8721
Prog Assignment: 1
Date: 10/11/2013
*/


#ifndef BOX_H
#define BOX_H

using namespace std;
#include "Vector3D.h"
#include "Scene.h"
#include "Ray.h"
#include "Shape.h"
#include "HitStructure.h"
#include "Matrix.h"
#include "InstancedObject.h"


namespace sivelab {

  class Box: public Shape {
  
  public:
    Vector3D a1, b1, c1, d1, e1, f1, g1, h1;
    vector<Shape *> boxTriangles;
    string boxName, shaderRefName;
    Box();
   // Box(string, string, Vector3D, Vector3D); 
    void createBBox() {bb =new BoundingBox(minPt,maxPt);}
    bool intersect(Ray *,double&,double&,HitStructure*);
    void generateTriangles();
    void generateBaseTriangles();
    void generateInstanceTriangles(string,string,Shader*,Matrix*);
    
  };
  
  
  
}

#endif
