/*
  File: Triangle.h

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013
*/

#ifndef TRIANGLE_H
#define TRIANGLE_H

#include<iostream>
#include "Ray.h"
#include "Shape.h"
#include "Vector3D.h"
#include "Scene.h"
#include "HitStructure.h"

using namespace std;

namespace sivelab {

class Triangle : public Shape{

public:
double a,b,c,d,e,f,g,h,i,j,k,l,beta,gamma,t,tmin,det;
Vector3D origin,direction;
Scene *sc;
Triangle();
Triangle(string);
~Triangle();
void createBBox();
bool intersect(Ray *,double&,double&, HitStructure *);

};
}

#endif
