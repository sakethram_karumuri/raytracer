/*
  File: LightPoint.h

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013
*/

#ifndef LIGHTPOINT_H
#define LIGHTPOINT_H

#include<iostream>
#include "Vector3D.h"
#include "Light.h"

using namespace std;

namespace sivelab
{

  class LightPoint: public Light {

  public:

    LightPoint();
    LightPoint(Vector3D,Vector3D,string);
    
    Vector3D getPosition() {return position;}

  };

}

#endif
