/*
  File: HitStructure.h

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013

Description: This class holds few key information that is required for computing shaders, shadows, etc.. This class's object is passed between the functions. 
*/


#ifndef HITSTRUCTURE_H
#define HITSTRUCTURE_H

using namespace std;
#include <cstdlib>
#include <limits>
#include "Vector3D.h"
//#include "Shape.h"

namespace sivelab {
class Shape;
  class HitStructure {
  public:

    double tfinal;
    int firstObjref;
    Vector3D normal,ptContact,view,light,capL,reflec,l_mirror;
    int shaderRef;
    Shape *firstObj;


    HitStructure();


  };

}

#endif
