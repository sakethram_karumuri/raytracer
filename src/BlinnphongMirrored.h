/*
File: BlinnphongMirrored.h

Instructor: Pete Willemsen
Student: Sakethram Karumuri
Course: CS 8721
Prog Assignment: 1
Date: 10/11/2013
*/


#ifndef BLINNPHONGMIRRORED_H
#define BLINNPHONGMIRRORED_H

using namespace std;
#include<iostream>
#include "HitStructure.h"
#include "Ray.h"
#include "Vector3D.h"
#include "Scene.h"
#include "Shader.h"
#include "RenderObject.h"

namespace sivelab {
  class BlinnphongMirrored: public Shader {
  
  public:
    Scene *sc;
    
    //double mirrCoef;
    BlinnphongMirrored();
    BlinnphongMirrored(string,string,Vector3D,Vector3D,float,double);
    Vector3D applyShading(Ray *,double,double,HitStructure *,Vector3D);
    
    
    
  };
  
}

#endif
