/*
File: Box.cpp

Instructor: Pete Willemsen
Student: Sakethram Karumuri
Course: CS 8721
Prog Assignment: 1
Date: 10/11/2013

Description: This class does the activity of splitting the faces of boxes into 12 triangles.
*/

#include <iostream>
#include "Box.h"
#include "Triangle.h"

using namespace sivelab;

Box::Box() {
}

bool Box::intersect(Ray *r, double& tmin, double& tmax, HitStructure *hs) {
/*
  bool hit;
  for(int i=0;i<boxTriangles.size();i++) {
  
    if(boxTriangles[i]->intersect(r,tmin,tmax,hs))
      hit=true;
  }
  
  return hit;
  */
  return false;
}


void Box::generateTriangles() {

  
  boxName = name;
  shaderRefName = ref;
  Scene *sc=Scene::getInstance();
  //cout<<"Box called"<<endl;
  //cout<<"Shader ref inside box is: "<<shaderRef->name<<endl;
 // cout<<"Min point and Max point are:"<<minPt<<" "<<maxPt<<endl;
  //Setting the vertices from min point and max point
	a1=minPt;
	b1.set(maxPt[0],minPt[1],minPt[2]);
	c1.set(maxPt[0],minPt[1],maxPt[2]);
	d1.set(minPt[0],minPt[1],maxPt[2]);
	e1.set(minPt[0],maxPt[1],maxPt[2]);
	f1.set(minPt[0],maxPt[1],minPt[2]);
	g1.set(maxPt[0],maxPt[1],minPt[2]);
	h1=maxPt;
	
      //cout<<"Inside box, the shader ref being used is: "<<shaderRef->name<<endl;
    /////////////////
    // z values same
    //front lower
	
	
    Shape *dch = new Triangle();
    dch->name= boxName; dch->type= "triangle"; dch->ref= shaderRefName; dch->shaderRef=shaderRef;
    dch->v0=d1; dch->v1=c1; dch->v2=h1;
    dch->orientation="lower";
    //boxTriangles.push_back(dch);
    sc->shapeObjects.push_back(dch);
    
    //front upper
    Shape *hed = new Triangle();
    hed->name= boxName; hed->type= "triangle"; hed->ref= shaderRefName; hed->shaderRef=shaderRef;
    hed->v0=h1; hed->v1=e1; hed->v2=d1;
    hed->orientation="upper";
    //boxTriangles.push_back(hed);
    sc->shapeObjects.push_back(hed);
    
    //back lower
    Shape *baf = new Triangle();
    baf->name= boxName; baf->type= "triangle"; baf->ref= shaderRefName; baf->shaderRef=shaderRef;
    baf->v0=b1; baf->v1=a1; baf->v2=f1;
    baf->orientation="lower";
    //boxTriangles.push_back(baf);
    sc->shapeObjects.push_back(baf);
   
    //back upper
    Shape *fgb = new Triangle();
    fgb->name= boxName; fgb->type= "triangle"; fgb->ref= shaderRefName; fgb->shaderRef=shaderRef;
    fgb->v0=f1; fgb->v1=g1; fgb->v2=b1;
    fgb->orientation="upper";
    //boxTriangles.push_back(fgb);
    sc->shapeObjects.push_back(fgb);
    
   
    //////////////////
    // x values same
    //right lower
    Shape *cbg = new Triangle();
    cbg->name= boxName; cbg->type= "triangle"; cbg->ref= shaderRefName; cbg->shaderRef=shaderRef;
    cbg->v0=c1; cbg->v1=b1; cbg->v2=g1;
    cbg->orientation="lower";
    //boxTriangles.push_back(cbg);
    sc->shapeObjects.push_back(cbg);
    
    //right upper
    Shape *ghc = new Triangle();
    ghc->name= boxName; ghc->type= "triangle"; ghc->ref= shaderRefName; ghc->shaderRef=shaderRef;
    ghc->v0=g1; ghc->v1=h1; ghc->v2=c1;
    ghc->orientation="upper";
    //boxTriangles.push_back(ghc);
    sc->shapeObjects.push_back(ghc);
    
     //left lower
    Shape *ade = new Triangle();
    ade->name= boxName; ade->type= "triangle"; ade->ref= shaderRefName; ade->shaderRef=shaderRef;
    ade->v0=a1; ade->v1=d1; ade->v2=e1;
    ade->orientation="lower";
    //boxTriangles.push_back(ade);
    sc->shapeObjects.push_back(ade);
    
    //left upper
    Shape *efa = new Triangle();
    efa->name= boxName; efa->type= "triangle"; efa->ref= shaderRefName; efa->shaderRef=shaderRef;
    efa->v0=e1; efa->v1=f1; efa->v2=a1;
    efa->orientation="upper";
    //boxTriangles.push_back(efa);
    sc->shapeObjects.push_back(efa);
    
    ///////////
    // y values same
    
    //top lower
    Shape *ehg = new Triangle();
    ehg->name= boxName; ehg->type= "triangle"; ehg->ref= shaderRefName; ehg->shaderRef=shaderRef;
    ehg->v0=e1; ehg->v1=h1; ehg->v2=g1;
    ehg->orientation="lower";
    //boxTriangles.push_back(ehg); 
    sc->shapeObjects.push_back(ehg);
    
    //top upper
    Shape *gfe = new Triangle();
    gfe->name= boxName; gfe->type= "triangle"; gfe->ref= shaderRefName; gfe->shaderRef=shaderRef;
    gfe->v0=g1; gfe->v1=f1; gfe->v2=e1;
    gfe->orientation="upper";
    //boxTriangles.push_back(gfe);
    sc->shapeObjects.push_back(gfe);
    
    //bottom lower
    Shape *cda = new Triangle();
    cda->name= boxName; cda->type= "triangle"; cda->ref= shaderRefName; cda->shaderRef=shaderRef;
    cda->v0=c1; cda->v1=d1; cda->v2=a1;
    cda->orientation="lower";
    //boxTriangles.push_back(cda);
    sc->shapeObjects.push_back(cda);
    
    //bottom upper
    Shape *abc = new Triangle();
    abc->name= boxName; abc->type= "triangle"; abc->ref= shaderRefName; abc->shaderRef=shaderRef;
    abc->v0=a1; abc->v1=b1; abc->v2=c1;
    abc->orientation="upper";
    //boxTriangles.push_back(abc);
    sc->shapeObjects.push_back(abc);

}


void Box::generateBaseTriangles() {

  boxName = name;
  shaderRefName = ref;
  Scene *sc=Scene::getInstance();
  //cout<<"Box called"<<endl;
  //cout<<"Shader ref inside box is: "<<shaderRef->name<<endl;
 // cout<<"Min point and Max point are:"<<minPt<<" "<<maxPt<<endl;
  //Setting the vertices from min point and max point
	a1=minPt;
	b1.set(maxPt[0],minPt[1],minPt[2]);
	c1.set(maxPt[0],minPt[1],maxPt[2]);
	d1.set(minPt[0],minPt[1],maxPt[2]);
	e1.set(minPt[0],maxPt[1],maxPt[2]);
	f1.set(minPt[0],maxPt[1],minPt[2]);
	g1.set(maxPt[0],maxPt[1],minPt[2]);
	h1=maxPt;
	
      //cout<<"Inside box, the shader ref being used is: "<<shaderRef->name<<endl;
    /////////////////
    // z values same
    //front lower
	
	
    Shape *dch = new Triangle();
    dch->name= boxName; dch->type= "triangle"; dch->ref= shaderRefName; dch->shaderRef=shaderRef;
    dch->v0=d1; dch->v1=c1; dch->v2=h1;
    dch->orientation="lower";
    //sc->baseObjects.push_back(dch);
     sc->baseObjMap.insert(pair<std::string,Shape *>("dch",dch));
     dch->createBBox();
    
    //front upper
    Shape *hed = new Triangle();
    hed->name= boxName; hed->type= "triangle"; hed->ref= shaderRefName; hed->shaderRef=shaderRef;
    hed->v0=h1; hed->v1=e1; hed->v2=d1;
    hed->orientation="upper";
    //sc->shapeObjects.push_back(hed);
    sc->baseObjMap.insert(pair<std::string,Shape *>("hed",hed));
    hed->createBBox();
    
    //back lower
    Shape *baf = new Triangle();
    baf->name= boxName; baf->type= "triangle"; baf->ref= shaderRefName; baf->shaderRef=shaderRef;
    baf->v0=b1; baf->v1=a1; baf->v2=f1;
    baf->orientation="lower";
    //sc->shapeObjects.push_back(baf);
    sc->baseObjMap.insert(pair<std::string,Shape *>("baf",baf));
    baf->createBBox();
   
    //back upper
    Shape *fgb = new Triangle();
    fgb->name= boxName; fgb->type= "triangle"; fgb->ref= shaderRefName; fgb->shaderRef=shaderRef;
    fgb->v0=f1; fgb->v1=g1; fgb->v2=b1;
    fgb->orientation="upper";
    //sc->shapeObjects.push_back(fgb);
    sc->baseObjMap.insert(pair<std::string,Shape *>("fgb",fgb));
    fgb->createBBox();
    
   
    //////////////////
    // x values same
    //right lower
    Shape *cbg = new Triangle();
    cbg->name= boxName; cbg->type= "triangle"; cbg->ref= shaderRefName; cbg->shaderRef=shaderRef;
    cbg->v0=c1; cbg->v1=b1; cbg->v2=g1;
    cbg->orientation="lower";
    //sc->shapeObjects.push_back(cbg);
    sc->baseObjMap.insert(pair<std::string,Shape *>("cbg",cbg));
    cbg->createBBox();
    
    //right upper
    Shape *ghc = new Triangle();
    ghc->name= boxName; ghc->type= "triangle"; ghc->ref= shaderRefName; ghc->shaderRef=shaderRef;
    ghc->v0=g1; ghc->v1=h1; ghc->v2=c1;
    ghc->orientation="upper";
    //sc->shapeObjects.push_back(ghc);
    sc->baseObjMap.insert(pair<std::string,Shape *>("ghc",ghc));
    ghc->createBBox();
    
     //left lower
    Shape *ade = new Triangle();
    ade->name= boxName; ade->type= "triangle"; ade->ref= shaderRefName; ade->shaderRef=shaderRef;
    ade->v0=a1; ade->v1=d1; ade->v2=e1;
    ade->orientation="lower";
    //sc->shapeObjects.push_back(ade);
    sc->baseObjMap.insert(pair<std::string,Shape *>("ade",ade));
    ade->createBBox();
    
    //left upper
    Shape *efa = new Triangle();
    efa->name= boxName; efa->type= "triangle"; efa->ref= shaderRefName; efa->shaderRef=shaderRef;
    efa->v0=e1; efa->v1=f1; efa->v2=a1;
    efa->orientation="upper";
    //sc->shapeObjects.push_back(efa);
    sc->baseObjMap.insert(pair<std::string,Shape *>("efa",efa));
    efa->createBBox();
    
    ///////////
    // y values same
    
    //top lower
    Shape *ehg = new Triangle();
    ehg->name= boxName; ehg->type= "triangle"; ehg->ref= shaderRefName; ehg->shaderRef=shaderRef;
    ehg->v0=e1; ehg->v1=h1; ehg->v2=g1;
    ehg->orientation="lower";
    //sc->shapeObjects.push_back(ehg); 
    sc->baseObjMap.insert(pair<std::string,Shape *>("ehg",ehg));
    ehg->createBBox();
    
    
    //top upper
    Shape *gfe = new Triangle();
    gfe->name= boxName; gfe->type= "triangle"; gfe->ref= shaderRefName; gfe->shaderRef=shaderRef;
    gfe->v0=g1; gfe->v1=f1; gfe->v2=e1;
    gfe->orientation="upper";
    //sc->shapeObjects.push_back(gfe);
    sc->baseObjMap.insert(pair<std::string,Shape *>("gfe",gfe));
    gfe->createBBox();
    
    
    //bottom lower
    Shape *cda = new Triangle();
    cda->name= boxName; cda->type= "triangle"; cda->ref= shaderRefName; cda->shaderRef=shaderRef;
    cda->v0=c1; cda->v1=d1; cda->v2=a1;
    cda->orientation="lower";
    //sc->shapeObjects.push_back(cda);
    sc->baseObjMap.insert(pair<std::string,Shape *>("cda",cda));
    cda->createBBox();
    
    
    //bottom upper
    Shape *abc = new Triangle();
    abc->name= boxName; abc->type= "triangle"; abc->ref= shaderRefName; abc->shaderRef=shaderRef;
    abc->v0=a1; abc->v1=b1; abc->v2=c1;
    abc->orientation="upper";
    //sc->shapeObjects.push_back(abc);
    sc->baseObjMap.insert(pair<std::string,Shape *>("abc",abc));
    abc->createBBox();
    

}


void Box::generateInstanceTriangles(string name,string type,Shader* shadRef,Matrix* trans) {

  Scene *sc =Scene::getInstance();
  
  Shape *sp1 = new InstancedObject(trans);
  sp1->name=name;
  sp1->type=type;
  sp1->shaderRef=shadRef;
  sp1->baseRef= sc->baseObjMap.find("dch")->second;
 
  sc->shapeObjects.push_back(sp1);
  
 
  Shape *sp2 = new InstancedObject(trans);
  sp2->name=name;
  sp2->type=type;
  sp2->shaderRef=shadRef;
  sp2->baseRef= sc->baseObjMap.find("hed")->second;
  sc->shapeObjects.push_back(sp2);
  
  Shape *sp3 = new InstancedObject(trans);
  sp3->name=name;
  sp3->type=type;
  sp3->shaderRef=shadRef;
  sp3->baseRef= sc->baseObjMap.find("baf")->second;
  sc->shapeObjects.push_back(sp3);
  
  Shape *sp4 = new InstancedObject(trans);
  sp4->name=name;
  sp4->type=type;
  sp4->shaderRef=shadRef;
  sp4->baseRef= sc->baseObjMap.find("fgb")->second;
  sc->shapeObjects.push_back(sp4);
  
  //////////////////
  
  Shape *sp5 = new InstancedObject(trans);
  sp5->name=name;
  sp5->type=type;
  sp5->shaderRef=shadRef;
  sp5->baseRef= sc->baseObjMap.find("cbg")->second;
  sc->shapeObjects.push_back(sp5);
  
  Shape *sp6 = new InstancedObject(trans);
  sp6->name=name;
  sp6->type=type;
  sp6->shaderRef=shadRef;
  sp6->baseRef= sc->baseObjMap.find("ghc")->second;
  sc->shapeObjects.push_back(sp6);
  
  Shape *sp7 = new InstancedObject(trans);
  sp7->name=name;
  sp7->type=type;
  sp7->shaderRef=shadRef;
  sp7->baseRef= sc->baseObjMap.find("ade")->second;
  sc->shapeObjects.push_back(sp7);
  
  Shape *sp8 = new InstancedObject(trans);
  sp8->name=name;
  sp8->type=type;
  sp8->shaderRef=shadRef;
  sp8->baseRef= sc->baseObjMap.find("efa")->second;
  sc->shapeObjects.push_back(sp8);
  
  //////////////////

  Shape *sp9 = new InstancedObject(trans);
  sp9->name=name;
  sp9->type=type;
  sp9->shaderRef=shadRef;
  sp9->baseRef= sc->baseObjMap.find("ehg")->second;
  sc->shapeObjects.push_back(sp9);
  
  Shape *sp10 = new InstancedObject(trans);
  sp10->name=name;
  sp10->type=type;
  sp10->shaderRef=shadRef;
  sp10->baseRef= sc->baseObjMap.find("gfe")->second;
  sc->shapeObjects.push_back(sp10);
  
  Shape *sp11 = new InstancedObject(trans);
  sp11->name=name;
  sp11->type=type;
  sp11->shaderRef=shadRef;
  sp11->baseRef= sc->baseObjMap.find("cda")->second;
  sc->shapeObjects.push_back(sp11);
  
  Shape *sp12 = new InstancedObject(trans);
  sp12->name=name;
  sp12->type=type;
  sp12->shaderRef=shadRef;
  sp12->baseRef= sc->baseObjMap.find("abc")->second;
  sc->shapeObjects.push_back(sp12);

 
}
