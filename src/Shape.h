/*
  File: Shape.h

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013

Description: It contains the variables that are used by all the objects.
*/

#ifndef SHAPE_H
#define SHAPE_H

#include<iostream>
#include "Vector3D.h"
#include "Ray.h"
#include "Shader.h"
#include "HitStructure.h"
#include "Matrix.h"
#include "BoundingBox.h"
#include <cstdlib>
#include <algorithm>
using namespace std;

namespace sivelab {

class Shape {

public:
string name,type,ref,orientation,id;
double radius;
double red,green,blue;
Vector3D center,minPt,maxPt,v0,v1,v2;
Shader* shaderRef;
BoundingBox* bb;
Shape* baseRef;

Shape();
virtual void createBBox() {}
virtual bool intersect(Ray *,double&,double&, HitStructure *) =0;
virtual void generateTriangles() {}
virtual void generateBaseTriangles() {}
virtual void generateInstanceTriangles(string,string,Shader*,Matrix*) {}
virtual void load(string&) {}
 ~Shape(){}

};

}

#endif
