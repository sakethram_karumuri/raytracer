/*
  File: Mirror.cpp

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013

Description: This class generates reflected rays when the camera rays hit a mirrored surface. It also checks if the reflected ray hits another object and if it hits, it applies the shader of that object to the mirror.
*/



#include "Mirror.h"

using namespace sivelab;

Mirror::Mirror() {

}

Mirror::Mirror(string nam,string ty) {

  name=nam;
  type=ty;
}

Vector3D Mirror::applyShading(Ray *r,double tmin, double tmax, HitStructure *hs,Vector3D camPosition) {
  
  Vector3D l_final;
  sc=Scene::getInstance();
  Ray *refRay = new Ray();
  bool firstObj;
  HitStructure *hsMirror = new HitStructure();
  
  refRay->type = "reflection";
  refRay->origin = hs->ptContact;

  hs->view= camPosition - hs->ptContact;
  hs->view.normalize();
  

  refRay->direction = (-1)*hs->view + 2*(hs->view.dot(hs->normal))*hs->normal;
  
  if(isRoughness) {
    double u,v;
    UVWbasis *basis = new UVWbasis(hs->ptContact,refRay->direction);
    Vec2D tempVec = sc->glossVector.back();
    u = (roughness/-2) + tempVec.x*roughness;
    v = (roughness/-2) + tempVec.y*roughness;
    refRay->direction = refRay->direction + (u*basis->u) + (v*basis->v);
    delete basis;
  }
  
  double tmin_reflection = 1.0e-03;
  double tmax_reflection = numeric_limits<double>::max();
  RenderObject *ro = new RenderObject();

  firstObj = ro->intersect(refRay,tmin_reflection,tmax_reflection,hsMirror);

  //returns background color if nothing is hit.
  if(!firstObj){
     l_final = sc->enviMapping(refRay);
    return l_final;
  }
  //checks depth and computes the shader info by recursively calling the applyShader function
  else {
     sc->depth -= 1;
       if(sc->depth<0){
      l_final = sc->enviMapping(refRay);
       return l_final;
       }
       else{
    hsMirror->firstObjref=firstObj;
    camPosition = hs->ptContact;
    ro->applyShader(refRay,hsMirror,camPosition);
    l_final.set(1,1,1);
    //return l_final;
    return hsMirror->capL;
    //}
  }
  
  
  //return temp;
}
}