/*
  File: Glaze.cpp

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013

Description: This class applies Glaze shader to the associated shape.
*/


#include "Glaze.h"

using namespace sivelab;

Glaze::Glaze(){
}

Glaze::Glaze(string nam,string ty, Vector3D diff, double mirr) {
  name = nam;
  type= ty;
  diffuse = diff;
  mirrorCoef = mirr;
}

// Returns the L value obtained from the calculations to the calling function
Vector3D Glaze::applyShading(Ray *r,double tmin, double tmax, HitStructure *hs,Vector3D camPosition) {
  
  Vector3D l_final,l_lamb;
   Lambertian *lam = new Lambertian(name,type,diffuse,diffuseRef);
     l_lamb = lam->applyShading(r,1.0e-03,numeric_limits<double>::max(),hs,camPosition);
  
     //cout<<l_lamb<<endl;
     //cout<<"Inside Glaze:"<<l_lamb<<endl;
     
  sc=Scene::getInstance();
  Ray *refRay = new Ray();
  bool firstObj;
  HitStructure *hsMirror = new HitStructure();
  double texel_s,texel_t;
  refRay->type = "reflection";
  refRay->origin = hs->ptContact;
 
  hs->view= camPosition - hs->ptContact;
  hs->view.normalize();

  refRay->direction = (-1)*hs->view + 2*(hs->view.dot(hs->normal))*hs->normal;

  if(isRoughness) {
    double u,v;
    Vector3D temp;
    UVWbasis *basis = new UVWbasis(hs->ptContact,temp,refRay->direction);
    Vec2D tempVec = sc->glossVector.back();
    u = (roughness/-2) + tempVec.x*roughness;
    v = (roughness/-2) + tempVec.y*roughness;
    refRay->direction = refRay->direction + (u*basis->u) + (v*basis->v);
    delete basis;
  }
  
  double tmin_reflection = 1.0e-03;
  double tmax_reflection = numeric_limits<double>::max();
  RenderObject *ro = new RenderObject();

  firstObj = ro->intersect(refRay,tmin_reflection,tmax_reflection,hsMirror);

  
  if(!firstObj){
    l_final = sc->enviMapping(refRay);
    //return l_final;    
l_lamb+= l_final;
    
  }
  else {
    //hsMirror->firstObjRef=firstObj;
    camPosition = hs->ptContact;   
    ro->applyShader(refRay,hsMirror,camPosition);
    l_final.set(1,1,1);
  
    l_lamb+= mirrorCoef*hsMirror->capL;
    
  }
  
  return l_lamb;
  
}
