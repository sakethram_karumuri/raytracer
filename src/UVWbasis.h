/*
  File: UVWbasis.h

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013
*/

#ifndef UVW_H
#define UVW_H

#include<iostream>
#include "Vector3D.h"
using namespace std;

namespace sivelab {

class UVWbasis {

public:

Vector3D u,v,w,lookatRay,temp;
Vector3D camPosition,lookatPoint,viewDir;
double lenu,lenv,lenw;

UVWbasis();
UVWbasis(Vector3D,Vector3D,Vector3D);
UVWbasis(Vector3D,Vector3D);


};

}

#endif
