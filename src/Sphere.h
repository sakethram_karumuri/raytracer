/*
  File: Sphere.h

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013
*/


#ifndef SPHERE_H
#define SPHERE_H

#include "Ray.h"
#include "Shape.h"
#include "Scene.h"

//#include "Vector3D.h"
#include "HitStructure.h"

using namespace std;

namespace sivelab {

class Sphere: public Shape
{
 public:
  double a,b,c,t1,t2,dis;

 Sphere();
 void createBBox();
 bool intersect(Ray *,double&,double&,HitStructure *);
 
};
}

#endif
