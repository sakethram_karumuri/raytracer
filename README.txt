Instructions to run the project:

The following functionalities are implemented in this assignment:
-Object Instancing
-objMesh
-BVH
-Anti aliasing
-Roughness
-Area Lights, soft shadows

1)The executable file is tests/Main.cpp
2)RPP should be mentioned using -r argument.
3)The use of BVH is optional. Everything runs perfectly using BVH. However without using BVH, there are small issues with mirror that needs to be resolved.

Eg: For using BVH, type -s yes (works fine in all the cases)
    For not using BVH, type -s no

4)Outputfile can be redirected using -o <filename> argument.
