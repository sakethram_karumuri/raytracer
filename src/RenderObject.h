/*
  File: RenderObject.h

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013
*/


#ifndef RENDEROBJECT_H
#define RENDEROBJECT_H

#include "Ray.h"
#include "Shape.h"
#include "Scene.h"
#include "HitStructure.h"
#include "algorithm"
#include <cmath>
#include "Vector3D.h"
#include "Lambertian.h"
#include "Blinnphong.h"
#include "Mirror.h"
#include "Glaze.h"
#include "BlinnphongMirrored.h"
#include <limits>

using namespace std;

namespace sivelab {

class RenderObject {
public:
Scene *sc;
Shape *sp;

 double a,b,c,det,t1,t2,tfinal;
 Vector3D minPt, maxPt, a1, b1, c1, d1, e1, f1, g1, h1;
 int before_size, after_size;
RenderObject();

 int intersect(Ray*,double&,double&, HitStructure*);
void applyShader(Ray*, HitStructure*, Vector3D);
bool intersectShadow (Ray *,double&,double&, HitStructure*);

};
}

#endif
