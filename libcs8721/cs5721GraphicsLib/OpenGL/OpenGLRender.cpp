#include "OpenGLRender.h"

OpenGLRender::OpenGLRender(const int winWidth, const int winHeight)   
  : m_winWidth( winWidth ), 
    m_winHeight( winHeight )
{  
  std::cout << "OpenGLRender - creating window of (" << m_winWidth << " X " << m_winHeight << ")" << std::endl;

  // Create the main window using the width and height parameters from
  // our command line arguments.
  m_Window = new sf::Window(sf::VideoMode(m_winWidth, m_winHeight, 32), "SFML OpenGL");

  glewInit();

  // ////////////////////////////////////////////////////////////////////////////////////
  // load any geometry you might want here.
  // ////////////////////////////////////////////////////////////////////////////////////

  // ////////////////////////////////////////////////////////////////////////////////////
  // Set the active window before using OpenGL commands. For a
  // single window OpenGL program this really doesn't do much, but
  // if you did have multiple windows, you might need to "render"
  // different things to each window, and thus, you need to make
  // each "window app" active before issuing OpenGL commands.
  m_Window->setActive();

  // Set color and depth clear value
  glClearColor(0.5f, 0.5f, 0.5f, 1.0f);

  // Enable Z-buffer read and write
  // glEnable(GL_DEPTH_TEST);
  // glDepthMask(GL_TRUE);

  // 
  // Setup the projection
  // 
  glViewport(0, 0, m_winWidth, m_winHeight);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  float verticalFieldOfView = 60.0; // in degrees
  float aspectRatio = m_winWidth/(float)m_winHeight;
  float nearPlaneDist = 1.0;
  float farPlaneDist = 500.0;
  gluPerspective(verticalFieldOfView, aspectRatio, nearPlaneDist, farPlaneDist);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();


  // Tell OpenGL which way you've defined your triangles' winding
  glFrontFace(GL_CCW);

  // This demo uses OpenGL's lighting system, so first, we must enable
  // lighting
  glEnable(GL_LIGHTING);
  // and then, enable the lights we will use with the hardware. In
  // this case, that is light0.
  glEnable(GL_LIGHT0);

  GLfloat ambientlightIntensity[] = { 0.2f, 0.2f, 0.2f, 1.0f };
  GLfloat lightIntensity[] = { 1.0f, 1.0f, 1.0f, 1.0f };
  glLightfv(GL_LIGHT0, GL_AMBIENT, ambientlightIntensity);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, lightIntensity);

  // In this example, we're going to render just a few triangles to
  // show how its done.

  glGenBuffers(1, &m_triangleVBO);
  glBindBuffer(GL_ARRAY_BUFFER, m_triangleVBO);

  m_numVertices = 3;
  int numFloatsPerVertex = 3;
  int numColors = 3;  // for non-material shading
  int numFloatsPerColor = 3; 
  int numTextureCoords = 3;
  int numFloatsPerTextureCoord = 2;

  float* vertexColorList = new float[ m_numVertices * numFloatsPerVertex * numColors * numFloatsPerColor * 2 * 3 ];
  
  int tIdx = 0;

  // ////////////////////////
  // V0
  // The vertex data
  vertexColorList[ tIdx++ ] = 0.0f;      
  vertexColorList[ tIdx++ ] = 2.5f;      
  vertexColorList[ tIdx++ ] = -8.0f;     

  // then it's normal
  vertexColorList[ tIdx++ ] = 0.0f;
  vertexColorList[ tIdx++ ] = 0.0f;
  vertexColorList[ tIdx++ ] = 1.0f;

  // then, the texture coordinate if it has one
  vertexColorList[ tIdx++ ] = 0.0;
  vertexColorList[ tIdx++ ] = 0.0;

  // ////////////////////////
  // V1
  // the next vertex data
  vertexColorList[ tIdx++ ] = -2.5f;     
  vertexColorList[ tIdx++ ] = -2.5f;     
  vertexColorList[ tIdx++ ] = -5.0f;     

  // it's normal
  vertexColorList[ tIdx++ ] = 0.0f;
  vertexColorList[ tIdx++ ] = 0.0f;
  vertexColorList[ tIdx++ ] = 1.0f;

  // then, the texture coordinate
  vertexColorList[ tIdx++ ] = 1.0;
  vertexColorList[ tIdx++ ] = 0.0;

  // ////////////////////////
  // V2
  // the 3rd vertex
  vertexColorList[ tIdx++ ] = 2.5f;      
  vertexColorList[ tIdx++ ] = -2.5f;     
  vertexColorList[ tIdx++ ] = -5.0f;     

  // it's normal
  vertexColorList[ tIdx++ ] = 0.0f;
  vertexColorList[ tIdx++ ] = 0.0f;
  vertexColorList[ tIdx++ ] = 1.0f;

  // then, the texture coordinate
  vertexColorList[ tIdx++ ] = 0.5;
  vertexColorList[ tIdx++ ] = 0.5;

  int numBytes = m_numVertices * numFloatsPerVertex * numColors * numFloatsPerColor * 2 * 3 * sizeof(float);
  glBufferData(GL_ARRAY_BUFFER, numBytes, vertexColorList, GL_STATIC_DRAW);
  delete [] vertexColorList;

  glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void OpenGLRender::run()  
{  
  // Start rendering loop
  while (m_Window->isOpen()) {

    // Process events
    sf::Event Event;
    while (m_Window->pollEvent(Event)) {

      // Close window : exit
      if (Event.type == sf::Event::Closed) {
	m_Window->close();
      }

      // Escape key : exit
      if ((Event.type == sf::Event::KeyPressed) 
	  && (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))) {
	m_Window->close();
      }

      // Resize event : adjust viewport
      if (Event.type == sf::Event::Resized) {
	glViewport(0, 0, Event.size.width, Event.size.height);
      }

      // Check for W, A, S, D keys...
      if ((Event.type == sf::Event::KeyPressed) 
	  && (sf::Keyboard::isKeyPressed(sf::Keyboard::W))) {
	// Move your camera forward
      }

      if (Event.type == sf::Event::MouseButtonPressed) {
	if (Event.mouseButton.button == sf::Mouse::Left) {
	  // Do something with the mouse data
	}
      }
    }

    // Set the active window before using OpenGL commands. For a
    // single window OpenGL program this really doesn't do much, but
    // if you did have multiple windows, you might need to "render"
    // different things to each window, and thus, you need to make
    // each "window app" active before issuing OpenGL commands.
    m_Window->setActive();

    // Place your OpenGL calls here that allow the screen to be
    // refreshed each time this loop is executed.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();    

    // gluLookAt(camera.getPos()[0], camera.getPos()[1], camera.getPos()[2],
    // camera.getLookatPt()[0], camera.getLookatPoint()[1], camera.getLookatPoint()[2],
    // camera.getUp()[0], camera.getUp()[1], camera.getUp()[2]);

    // Create a simple light for light0 and place it at a known
    // position. Notice the 4th coordinate is 1.0.
    GLfloat lightPos[4] = {10.0, 10.0, -10.0, 1.0};
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);

    // Create a simple, material-based cube. We need to define the material state:
    float white[4] = {1.0, 1.0, 1.0, 1.0};
    float red[4] = {1.0, 0.0, 0.0, 0.0};
    float ambient[4] = {0.2, 0.2, 0.2, 1.0};
    float phongExp[1] = {68.0};
  
    // Set the diffuse coefficient for the object to be red
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, red);

    // Set the ambient coefficient for the object to be dark grey
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient);

    // Set the specular coefficient for the object to be white
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, white);

    // Set the phong exponent
    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, phongExp);

    glEnableClientState(GL_VERTEX_ARRAY); 
      glEnableClientState(GL_NORMAL_ARRAY); 
      glEnableClientState(GL_TEXTURE_COORD_ARRAY); 
      glBindBuffer(GL_ARRAY_BUFFER, m_triangleVBO);

      glVertexPointer(3, GL_FLOAT, 8 * sizeof(float), 0);
      glNormalPointer(GL_FLOAT, 8 * sizeof(float), (GLvoid*)(3*sizeof(float)));
      glTexCoordPointer(2, GL_FLOAT, 8 * sizeof(float), (GLvoid*)(6*sizeof(float)));

      // glBindTexture(GL_TEXTURE_2D, texID);
      glDrawArrays(GL_TRIANGLES, 0, m_numVertices);
      // glBindTexture(GL_TEXTURE_2D, 0);

      glDisableClientState(GL_VERTEX_ARRAY);    
      glDisableClientState(GL_COLOR_ARRAY);    
      glDisableClientState(GL_TEXTURE_COORD_ARRAY); 
      glBindBuffer(GL_ARRAY_BUFFER, 0);

#if 0

    // Simplest triangle...
    glDisable(GL_LIGHTING);
    
    // Immediate Mode
    glColor3f(1.0, 0.0, 0.0);
    glBegin(GL_TRIANGLES);
    glVertex3f(-10, -10, -10);
    glVertex3f(10, -10, -10);
    glVertex3f(0, 10, -10);
    glEnd();
#endif
    
    // If you want to see what you render in wireframe, uncomment this line:
    // glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

    // for (all objects)
    // o->oglRender();

    // Finally, display rendered frame on screen - this is the command
    // that tells OpenGL to actually refresh the window you're looking
    // at with what you wrote to memory in the commands above.
    m_Window->display();
  }
}  
