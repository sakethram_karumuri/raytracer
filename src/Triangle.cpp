/*
  File: Triangle.cpp

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013

Description: This class has the intersection function for a triangle.
*/

#include "Triangle.h"

using namespace sivelab;

Triangle::Triangle() {

}

Triangle::Triangle(string trName) {
name = trName;
}

void Triangle::createBBox() {

  Vector3D minPoint,maxPoint;
  minPoint[0] = min(v0[0],min(v1[0],v2[0]));
  minPoint[1] = min(v0[1],min(v1[1],v2[1]));
  minPoint[2] = min(v0[2],min(v1[2],v2[2])); 
  
  maxPoint[0] = max(v0[0],max(v1[0],v2[0]));
  maxPoint[1] = max(v0[1],max(v1[1],v2[1]));
  maxPoint[2] = max(v0[2],max(v1[2],v2[2])); 
  
  
   //cout<<"Min point: "<<minPoint<<" Max point: "<<maxPoint<<endl;
   
  bb = new BoundingBox(minPoint,maxPoint);
}

bool Triangle::intersect(Ray *r, double& tmin, double& tmax, HitStructure *hs) {
  sc=Scene::getInstance();

  origin=r->origin;
  direction=r->direction;
//   v0=s->v0;
//   v1=s->v1;
//   v2=s->v2;

  //tmin=1;

 // cout<<v0<<v1<<v2<<endl;
  
  a=v0[0]-v1[0];
  b=v0[1]-v1[1];
  c=v0[2]-v1[2];
  d=v0[0]-v2[0];
  e=v0[1]-v2[1];
  f=v0[2]-v2[2];
  g=direction[0];
  h=direction[1];
  i=direction[2];
  j=v0[0]-origin[0];
  k=v0[1]-origin[1];
  l=v0[2]-origin[2];
  det= a*(e*i-h*f)-d*(b*i-c*h)+g*(b*f-c*e);

  //cout<<"det is:"<<det<<endl;

  t= f*(a*k-j*b)+e*(j*c-a*l)+d*(b*l-k*c);
  t/= (-1)*det;



  //cout<<"t is:"<<t<<endl;
 
  //if the triangle lies between camera and imageplane, ignore it. Also if it lies beyond tmax, ignore it.
  if(t<tmin ||t>tmax)
    return false;

  gamma= i*(a*k-j*b)+h*(j*c-a*l)+g*(b*l-k*c);
  gamma/= det;

  //cout<<"gamma is:"<<gamma<<endl;

  if(gamma<0||gamma>1)
    return false;

  beta= j*(e*i-h*f)+k*(g*f-d*i)+l*(d*h-e*g);
  beta/= det;

  //cout<<"beta is:"<<beta<<endl;

  if(beta<0|| beta>(1-gamma))
    return false;

  //cout<<"Vertex v1:"<<s->v0[0]<<" Vertex v2:"<<s->v1[1]<<" Vertex v3:"<<s->v2[2]<<endl;

  if(t<tmax){
    if(r->type != "shadow") {
    tmax=t;
    hs->tfinal=tmax;
    hs->ptContact=r->origin+ tmax*r->direction;
//cout<<"Inside shape: "<<this->name<<endl;
//cout<<"Point of contact in triangle is: "<< hs->ptContact<<endl;
    hs->normal=(v1-v0).cross(v2-v0);
    hs->normal.normalize();
    //cout<<"Normal inside triangle is: "<<hs->normal<<endl; 
   hs->firstObj=this;
    }
    return true;
    //sc->shapeObjects[
  }
  return false;
}
