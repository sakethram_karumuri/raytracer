/*
  File: Ray.h

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013
*/

#ifndef RAY_H
#define RAY_H

#include<iostream>
#include "Vector3D.h"
using namespace std;

namespace sivelab
{

  class Ray {

  public:

    Vector3D origin, tempRay, direction;
    double lengthRay;
    string type;

    Ray();
    Ray(Vector3D);
    Ray(const double,const double,const double);

    double lengthofRay(Vector3D);


  };

}

#endif
