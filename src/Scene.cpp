/*
  File: Scene.cpp

  Instructor: Pete Willemsen
  Student: Sakethram Karumuri
  Course: CS 8721
  Prog Assignment: 1
  Date: 10/11/2013
*/

#include "Scene.h"

using namespace sivelab;

Scene* Scene::single=NULL;

Scene::Scene() {
  //cout<<"New Scene object created"<<endl;
}

Scene* Scene::getInstance() {
  if(single==NULL){
    single=new Scene();
    return single;
  }
  else{
    //cout<<"Object exists"<<endl;
    return single;
  }
}

Vector3D Scene::textureLookup(double s,double t,string textureRef) {
 
  for(int i=0;i<textureObjects.size();i++) {
  
    //cout<<textureObjects.size()<<endl;
    if(textureObjects[i]->name==textureRef) {
	
      double texImageWidth = textureObjects[i]->image.get_width();
      double texImageHeight = textureObjects[i]->image.get_height();
      
      //cout<<"Image width: "<<texImageWidth<<endl;
      //cout<<"Image height: "<<texImageHeight<<endl;
      
      t = 1-t;
      //s=1-s;
      
      if(s>1||t>1) {
	if(s>1)
	  s=0.99;
	else if(t>1)
	  t=0.99;
      }
      
	
      int s_pixel = (int)(s*texImageWidth);
      int t_pixel = (int)(t*texImageHeight);
      
      //cout<<"s: "<<s<<"\tt: "<<t;
      //cout<<"\ttexel x: "<<s_pixel<<"\ttexel y: "<<t_pixel<<endl;
     
      double red = (double)textureObjects[i]->image.get_pixel(s_pixel,t_pixel).red;
      red /= 255;
       
      double green = (double)textureObjects[i]->image.get_pixel(s_pixel,t_pixel).green;
      green /= 255;
      double blue = (double)textureObjects[i]->image.get_pixel(s_pixel,t_pixel).blue;
      blue /= 255;
     
      //  cout<<red<<green<<blue<<endl;
      kd.set(red,green,blue);
      //cout<<kd<<endl;
     
      return kd;
      break;
    
      
    }
    
  }
  
}

void Scene::computeSTSphere(Vector3D ptContact,Shape* objRef,double* s,double* t) {
  
  double theta;
  double phi;
  double temp1,temp2,temp3;
  
  temp1 = ptContact[1]-objRef->center[1];
  temp1 /= objRef->radius;
  
  temp2 = ptContact[2]-objRef->center[2];
  temp3 = ptContact[0]-objRef->center[0];
  
  theta = acos(temp1);
  phi = atan2(temp2,temp3);
  if(phi<0)
    phi += 6.28;
  
//  cout<<"Pi: "<<pi<<endl;
  *s = phi/6.28;
  *t = (3.14-theta)/3.14;
}


void Scene::computeSTTriangle(Vector3D ptContact,Shape* objRef,double* s,double* t) {

//Computed without using barycentric coordinates

  Vector3D v0 = objRef->v0;
  Vector3D v1 = objRef->v1;
  Vector3D v2 = objRef->v2;
  //cout<<"v0: "<<v0<<"\tv1: "<<v1<<"\tv2: "<<v2<<endl;
  //cout<<"ptContact: "<<ptContact<<endl;
  //cout<<"Inside computeSTTriangle"<<endl;
  
  if(v0[2]==v1[2] && v1[2]==v2[2]) {
  //cout<<"z is same"<<endl<<endl;
    *s = (ptContact[0]-v0[0])/(v1[0]-v0[0]);
    *t = (ptContact[1]-v0[1])/(v2[1]-v0[1]);
    }
  
  if(v0[0]==v1[0] && v1[0]==v2[0]) {
  //cout<<"x is same"<<endl<<endl;
    *s = (ptContact[2]-v0[2])/(v1[2]-v0[2]);
    *t = (ptContact[1]-v0[1])/(v2[1]-v0[1]);
    }
  
  if(v0[1]==v1[1] && v1[1]==v2[1]) {
    //cout<<"y is same"<<endl<<endl;
    *s = (ptContact[0]-v0[0])/(v1[0]-v0[0]);
    *t = (ptContact[2]-v0[2])/(v2[2]-v0[2]);
    }
  
  if(objRef->orientation=="upper") {
      *s = 1-*s;
      *t = 1-*t;
    }
  
  
  //cout<<*s<<" "<<*t<<endl;
}

Vector3D Scene::enviMapping(Ray* r) {
  //cout<<"Inside envi mapping"<<endl;
  
  if(enviMap=="") {
    bgColor.set(0,0,0);
  return bgColor;    
  }
  
  else {
  Vector3D dir = r->direction;
  double s,t,imWidth,imHeight;
  //cout<<dir<<endl;
  
  //X component greater
  if(abs(dir[0])>abs(dir[1]) && abs(dir[0])>abs(dir[2])) {
      //cout << "X component greater"<<endl;
      if(dir[0]>0) {
      //use posX image
	s = ((-1*dir[2])/abs(dir[0]))+1;
	s/=2;
	t = ((-1*dir[1])/abs(dir[0]))+1;
	t/=2;
	currentImage = &sp->posX;
	//cout<<"s:"<<s<<"\tt: "<<t<<endl;
      }
      
      else if(dir[0]<0) {
	s = (dir[2]/abs(dir[0]))+1;
	s/=2;
	t = ((-1*dir[1])/abs(dir[0]))+1;
	t/=2;
	currentImage = &sp->negX;
		
      }      
  }
  
  //Y component greater
  else if(abs(dir[1])>abs(dir[0]) && abs(dir[1])>abs(dir[2])) {
      //cout << "Y component greater"<<endl;
        if(dir[1]>0) {
      //use posY image
	s = (dir[0]/abs(dir[1]))+1;
	s/=2;
	t = (dir[2]/abs(dir[1]))+1;
	t/=2;
	currentImage = &sp->posY;
	//cout<<"s:"<<s<<"\tt: "<<t<<endl;
      }
      else if(dir[1]<0) {
	s = (dir[0]/abs(dir[1]))+1;
	s/=2;
	t = ((-1*dir[2])/abs(dir[1]))+1;
	t/=2;
	currentImage = &sp->negY;

	//cout<<"s:"<<s<<"\tt: "<<t<<endl;

	
      }
  }
  
  //Z component greater
  else if(abs(dir[2])>abs(dir[0]) && abs(dir[2])>abs(dir[1])) {
      //cout << "Z component greater"<<endl;
       if(dir[2]>0) {
      //use posZ image
	s = (dir[0]/abs(dir[2]))+1;
	s/=2;
	t = ((-1*dir[1])/abs(dir[2]))+1;
	t/=2;
	currentImage = &sp->posZ;
	//cout<<"s:"<<s<<"\tt: "<<t<<endl;
      }
      else if(dir[2]<0) {
	s = ((-1*dir[0])/abs(dir[2]))+1;
	s/=2;
	t = ((-1*dir[1])/abs(dir[2]))+1;
	t/=2;
	currentImage = &sp->negZ;
	//cout<<"s:"<<s<<"\tt: "<<t<<endl;
	
      }
  }
  
  imWidth = (*currentImage).get_width();
  imHeight = (*currentImage).get_height();
  
  //imWidth = sp->posX.get_width();
  //imHeight = sp->posX.get_height();
   int s_pixel = (int)(s*imWidth);
   int t_pixel = (int)(t*imHeight);
   
   double red = (double)(*currentImage).get_pixel(s_pixel,t_pixel).red;
   red /= 255;
   double green = (double)(*currentImage).get_pixel(s_pixel,t_pixel).green;
   green /= 255;
   double blue = (double)(*currentImage).get_pixel(s_pixel,t_pixel).blue;
   blue /= 255;
      
   bgColor.set(red,green,blue);
    
  return bgColor;
  }
}

double Scene::noise(double x,double y) {

 //random noise function found on internet
 //It returns a double value in the range -1 to 1
   int n=(int)x+(int)y*57;
   n=(n<<13)^n;
   int nn=(n*(n*n*60493+19990303)+1376312589)&0x7fffffff;
   return 1.0-((double)nn/1073741824.0);
  
}

Vector3D Scene::perlinNoise(Vector3D ptContact) {
  Vector3D color(0,1,0),black(0,0,0),white(1,1,1);
 
  int k1=10,k2=20;
  double w=0.005;
  //double temp = noise((int)abs(ptContact[0]),ptContact[1]);
  //cout<<temp<<endl;
 // double temp2 = (1 + sin( (ptContact[0] + temp / 2 ) * 50) ) / 2;
  //temp=abs(temp2);
  //color.set(temp,temp,temp);
  double t = (1 + sin(k1*ptContact[2] + turbulance(k2*ptContact[0],k2*ptContact[1]))/w)/2;
  //color.set(t,t,t);
  color = (1-t)*black+t*white;
  return color;
}

double Scene::turbulance(double x,double y) {

  double turbulance;
  
  for(int i=0;i<20;i++) {  
    turbulance += (noise(pow(2,i)*x,pow(2,i)*y))/pow(2,i); 
  }
  return turbulance;
  //return 0;
}
