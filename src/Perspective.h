/*
File: Perspective.h

Instructor: Pete Willemsen
Student: Sakethram Karumuri
Course: CS 8721
Prog Assignment: 3
Date: 11/19/2013
*/


#ifndef PERSPECTIVE_H
#define PERSPECTIVE_H

#include<iostream>
#include "Camera.h"
#include "Vector3D.h"
#include "UVWbasis.h"
#include "Ray.h"
#include "png++/png.hpp"
#include <cstdlib>
#include "handleGraphicsArgs.h"



using namespace std;


namespace sivelab
{

class Perspective: public Camera {

public:

Perspective();
Perspective(string,Vector3D,Vector3D,Vector3D,float,float,double,double);

void computeRay(int,int, float, float);
};

}

#endif
